package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

/**
 * Created by marcel on 4/2/14.
 */
public class VisitorSet extends VisitQuery<Boolean> {
    public VisitorSet() {
        super();
    }
    final boolean DEBUG = Debug.enableSetOutput;
    @Override
    public Boolean visit(ExprBinary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Set): "+ x.toString() + " ExprBinary visited");
        VisitorExprType visitorExprType = new VisitorExprType();
        if(x.right.accept(visitorExprType).contains("ISet") || (x.left.accept(visitorExprType).contains("ISet"))) {
            return Boolean.TRUE;
        } else {
            switch (x.op) {

                case ARROW:
                case ANY_ARROW_SOME:
                case ANY_ARROW_ONE:
                case ANY_ARROW_LONE:
                case SOME_ARROW_ANY:
                case SOME_ARROW_SOME:
                case SOME_ARROW_ONE:
                case SOME_ARROW_LONE:
                case ONE_ARROW_ANY:
                case ONE_ARROW_SOME:
                case ONE_ARROW_ONE:
                case ONE_ARROW_LONE:
                case LONE_ARROW_ANY:
                case LONE_ARROW_SOME:
                case LONE_ARROW_ONE:
                case LONE_ARROW_LONE:
                    return Boolean.TRUE;
                case ISSEQ_ARROW_LONE:
                    break;
                case JOIN:
                    break;
                case DOMAIN:
                    break;
                case RANGE:
                    break;
                case INTERSECT:
                    break;
                case PLUSPLUS:
                    break;
                case PLUS:
                    break;
                case IPLUS:
                    break;
                case MINUS:
                    break;
                case IMINUS:
                    break;
                case MUL:
                    break;
                case DIV:
                    break;
                case REM:
                    break;
                case EQUALS:
                    break;
                case NOT_EQUALS:
                    break;
                case IMPLIES:
                    break;
                case LT:
                    break;
                case LTE:
                    break;
                case GT:
                    break;
                case GTE:
                    break;
                case NOT_LT:
                    break;
                case NOT_LTE:
                    break;
                case NOT_GT:
                    break;
                case NOT_GTE:
                    break;
                case SHL:
                    break;
                case SHA:
                    break;
                case SHR:
                    break;
                case IN:
                    break;
                case NOT_IN:
                    break;
                case AND:
                    break;
                case OR:
                    break;
                case IFF:
                    break;
            }
        }
        return Boolean.FALSE;
    }
    @Override
    public Boolean visit(ExprUnary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Set): "+ x.toString() + " ExprUnary visited");
        VisitorExprType visitorExprType = new VisitorExprType();
        //if(x.label)
        //return (Sig)x.sub;
        if(x.sub.accept(visitorExprType).contains("ISet")) {
            return Boolean.TRUE;
        } else {
            switch(x.op) {

                case SOMEOF:
                    return Boolean.TRUE;
                case LONEOF:
                    break;
                case ONEOF:
                    break;
                case SETOF:
                    return Boolean.TRUE;
                case EXACTLYOF:
                    break;
                case NOT:
                    break;
                case NO:
                    break;
                case SOME:
                    break;
                case LONE:
                    break;
                case ONE:
                    break;
                case TRANSPOSE:
                    break;
                case RCLOSURE:
                    break;
                case CLOSURE:
                    break;
                case CARDINALITY:
                    break;
                case CAST2INT:
                    break;
                case CAST2SIGINT:
                    break;
                case NOOP:
                    break;
            }
        }
        return Boolean.FALSE;
    }
    @Override
    public Boolean visit(Sig.Field x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Set): "+ x.toString() + " Field visited");
        VisitorExprType visitorExprType = new VisitorExprType();
        String expr = x.decl().expr.accept(visitorExprType);
        if(expr.contains("ISet")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    public Boolean visit(Sig x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Set): "+ x.toString() + " Sig visited");
        VisitorExprType visitorExprType = new VisitorExprType();
        String expr = x.accept(visitorExprType);
        if(expr.contains("ISet")) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
