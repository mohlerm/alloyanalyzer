package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorSig;

/**
 * Created by marcel on 3/23/14.
 */
public class TypeExprBinaryVisit {
    public static String visit (ExprBinary x, VisitorExprType visitor) throws Err {
        int depth = visitor.getDepth();


        StringBuilder res = new StringBuilder();
        StringBuilder operator = new StringBuilder();
        StringBuilder leftString = new StringBuilder();
        //leftString.append("(");
        StringBuilder rightString = new StringBuilder();
        //rightString.append(")");
        StringBuilder temp = new StringBuilder();
        VisitorSig vs = new VisitorSig();
        Sig.PrimSig ps;
        String[] nameArr;
        switch (x.op) {
            case ARROW:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ANY_ARROW_SOME:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ANY_ARROW_ONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ANY_ARROW_LONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case SOME_ARROW_ANY:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case SOME_ARROW_SOME:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case SOME_ARROW_ONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case SOME_ARROW_LONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ONE_ARROW_ANY:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ONE_ARROW_SOME:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ONE_ARROW_ONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case ONE_ARROW_LONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case LONE_ARROW_ANY:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case LONE_ARROW_SOME:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case LONE_ARROW_ONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            case LONE_ARROW_LONE:
                operator.append(", ");
                leftString.append("ISet<Tuple<");
                rightString.append(">>");
                break;
            //            case ISSEQ_ARROW_LONE:
            //                operator.append(",");
            //                leftString.append("ISet<Tuple<");
            //                rightString.append(">>");
            //                break;
            case JOIN:
                leftString.append("(");
                operator.append(".");
                rightString.append(")");
                temp.append(x.right.accept(visitor));
                break;
            //            case DOMAIN:
            //                break;
            //            case RANGE:
            //                break;
            case INTERSECT:
                nameArr = (((Sig.PrimSig)x.right.accept(vs)).intersect((Sig.PrimSig)x.left.accept(vs)).label).split("/");
                if(nameArr.length <= 1) {
                    if(nameArr[0].equals("univ")) {
                        temp.append("Object");
                    } else {
                        temp.append(nameArr[0].replace("}", "").replace("{", ""));
                    }
                } else {
                    temp.append(nameArr[1].replace("}", "").replace("{", ""));
                }
//                if (x.left.type().isSubtypeOf(x.right.type())) {
//                    temp.append(x.left.accept(visitor));
//
//                } else if (x.right.type().isSubtypeOf(x.left.type())) {
//                    temp.append(x.right.accept(visitor));
//                } else {
//                    VisitorSig vs = new VisitorSig();
//
//                    Sig.PrimSig ps = ((Sig.PrimSig)x.right.accept(vs)).leastParent((Sig.PrimSig)x.left.accept(vs));
//                    temp.append(ps.label);
//                }
                //} else {
                //   temp.append("Object");
                //}
                break;
            //            case PLUSPLUS:
            //                break;
            case PLUS:
                nameArr = (((Sig.PrimSig)x.right.accept(vs)).leastParent((Sig.PrimSig)x.left.accept(vs)).label).split("/");
                if(nameArr.length <= 1) {
                    if(nameArr[0].equals("univ")) {
                        temp.append("Object");
                    } else {
                        temp.append(nameArr[0].replace("}", "").replace("{", ""));
                    }
                } else {
                    temp.append(nameArr[1].replace("}", "").replace("{", ""));
                }
//                if (x.left.type().isSubtypeOf(x.right.type())) {
//                    temp.append(x.left.accept(visitor));
//
//                } else if (x.right.type().isSubtypeOf(x.left.type())) {
//                    temp.append(x.right.accept(visitor));
//                } else {
//                    temp.append("Object");
//                }
                break;

            case IPLUS:
                operator.append("+");
                leftString.append("(");
                rightString.append(")");
                break;
            case MINUS:
                // TODO: not implemented correctly
                nameArr = (((Sig.PrimSig)x.right.accept(vs)).leastParent((Sig.PrimSig)x.left.accept(vs)).label).split("/");
                if(nameArr.length <= 1) {
                    if(nameArr[0] == "univ") {
                        temp.append("Object");
                    } else {
                        temp.append(nameArr[0].replace("}", "").replace("{", ""));
                    }
                } else {
                    temp.append(nameArr[1].replace("}", "").replace("{", ""));
                }
                //temp.append(x.left.accept(visitor));
                break;
            case IMINUS:
                operator.append("-");
                leftString.append("(");
                rightString.append(")");
                break;
            case MUL:
                operator.append("*");
                leftString.append("(");
                rightString.append(")");
                break;
            case DIV:
                operator.append("/");
                leftString.append("(");
                rightString.append(")");
                break;
            case REM:
                operator.append("%");
                leftString.append("(");
                rightString.append(")");
                break;
            case EQUALS:
                operator.append("==");
                leftString.append("(");
                rightString.append(")");
                break;
            case NOT_EQUALS:
                operator.append("!=");
                leftString.append("(");
                rightString.append(")");
                break;
            case IMPLIES:
                operator.append("=>");
                leftString.append("(");
                rightString.append(")");
                break;
            case LT:
                operator.append("<");
                leftString.append("(");
                rightString.append(")");
                break;
            case LTE:
                operator.append("<=");
                leftString.append("(");
                rightString.append(")");
                break;
            case GT:
                operator.append(">");
                leftString.append("(");
                rightString.append(")");
                break;
            case GTE:
                operator.append(">=");
                leftString.append("(");
                rightString.append(")");
                break;
            case NOT_LT:
                operator.append("!<");
                leftString.append("(");
                rightString.append(")");
                break;
            case NOT_LTE:
                operator.append("!<=");
                leftString.append("(");
                rightString.append(")");
                break;
            case NOT_GT:
                operator.append("!>");
                leftString.append("(");
                rightString.append(")");
                break;
            case NOT_GTE:
                operator.append("!>=");
                leftString.append("(");
                rightString.append(")");
                break;
            //            case SHL:
            //                break;
            //            case SHA:
            //                break;
            //            case SHR:
            //                break;
            case IN:
                break;
            case NOT_IN:
                break;
            case AND:
                operator.append("&");
                leftString.append("(");
                rightString.append(")");
                break;
            case OR:
                operator.append("|");
                leftString.append("(");
                rightString.append(")");
                break;
            case IFF:
                break;
        }
        String leftSub = x.left.accept(visitor);
        String rightSub = x.right.accept(visitor);
        if (temp.length() == 0) {
            res.append(leftString).append(leftSub).append(operator).append(rightSub).append(rightString);
        } else {
            res.append(temp);
        }

        return new String(res);
    }
}
