package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;

/**
 * Created by marcel on 3/16/14.
 */
@Deprecated
public class ExprVarVisit {
    public static String visit(ExprVar exprVar, Visitor vis) throws Err {
        int depth = vis.getDepth();
        String offset = vis.getOffset();

        String exprVarName = exprVar.label;

        // definitions string contains exprVar definitions, contracts string contains contracts (e.g. invariants)
        StringBuilder definitions = new StringBuilder();
        StringBuilder contracts = new StringBuilder();

        // check for arity
        String[] typeArr = exprVar.type().toString().split("->");

        if (depth <= 1) {
            System.out.println("ERROR: SHOULD NOT HAPPEN");
//            // next line is for debug purpose only
//            //definitions.append("//exprVar ").append(exprVarName).append(" from type ").append(exprVar.type()).append("{\n");
//
//
//            // definitions setup with proper indentation
//            definitions.append(offset);
//            // if exprVar private
//            if (exprVar.isPrivate != null) {
//                definitions.append("private ");
//            }
//
//            // TODO implement proper subexpressions
//
//
//            // for binary and ternary relations
//
//            definitions.append("public ");
//            if (typeArr.length < 3) {
//                definitions.append(typeArr[1].split("/")[1].replace("}", ""));
//            }
//
//            contracts.append(offset);
//            if (exprVar.oneOf() != null) {
//                contracts.append("\tContract.Invariant(").append(exprVarName).append(" != null);\n");
//            }
//            //else if(exprVar.someOf() != null) {
//            //    definitions.append(" SOME");
//            //}
//            else if (exprVar.loneOf() != null) {
//                definitions.append(" LONE");
//            }
//
//            // if ternary relation
//            if (typeArr.length == 3) {
//                // figure out the sub expression names with the visitor
//
//                //definitions.append(offset).append(exprVar.decl().expr.accept(vis));
//                definitions.append("ISet<Tuple<").append(typeArr[1].split("/")[1].replace("}", "")).append(", ");
//                definitions.append(typeArr[2].split("/")[1].replace("}", "")).append(">>");
//                // also add new invariant
//                //System.out.println("DEBUG NAME: "+ exprVar.toString());
//
//                // TODO fix ternary invariants
//                VisitorObj visObj = new VisitorObj();
//                ExprObj exprObj = exprVar.decl().expr.accept(visObj);
//                String operator = exprObj.operator;
//                String leftType = null;
//                String leftName = null;
//                String rightType = null;
//                String rightName = null;
//                // if relation
//                if(operator.equals("->")) {
//                    // TODO needs to evaluate the complete tree (use while: nodes != null)
//                    //((null$.$Platform#floor)$->$(null$.$Platform#ceiling))
//                    leftType = exprObj.left.right.type; // in that case platform
//                    leftName = exprObj.left.right.name; // in that case floor
//                    // left.left is null with operand "."
//                    rightType = exprObj.right.right.type; // in that case platform
//                    rightName = exprObj.right.right.name; // in that case ceiling
//                    // right.left is null with operand "."
//
//
//
//                }
//
//                contracts.append("\t\tContract.Invariant(").append(exprVarName).append(" != null && ");
//                contracts.append(leftName).append(" != null && Contract.ForAll(").append(exprVarName);
//                contracts.append(", e1 => ").append(leftName).append(".Equals(e1.Item1)));\n");
//
//                contracts.append("\t\tContract.Invariant(").append(exprVarName).append(" != null && ");
//                contracts.append(rightName).append(" != null && Contract.ForAll(").append(exprVarName);
//                contracts.append(", e1 => ").append(rightName).append(".Equals(e1.Item2)));\n");
//
//            }
//
//            definitions.append(" ").append(exprVarName).append(";\n");
        }

        // if it's deep in the tree, just hand over type and name
        else {
            // TODO: remove that debug statement (and the whole branch is kinda useless)
            System.out.println("ERROR: SHOULD NOT HAPPEN ANYMORE");
//            definitions.append(typeArr[1].split("/")[1].replace("}", ""));
//            contracts.append(exprVarName);
        }


        return (new String(definitions) + "#" + new String(contracts));
    }
}


