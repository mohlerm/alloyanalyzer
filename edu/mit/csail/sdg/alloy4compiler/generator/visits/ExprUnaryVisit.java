package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;

/**
 * Created by marcel on 3/23/14.
 */
@Deprecated
public class ExprUnaryVisit {
    public static String visit (ExprUnary expr, Visitor vis) throws Err {
        int depth = vis.getDepth();
        System.out.println("ERROR: SHOULD NOT HAPPEN");
        StringBuilder ans = new StringBuilder();
        ans.append("(");
        ans.append(expr.op.toString());
        ans.append(")");

        return new String(ans);
    }
}
