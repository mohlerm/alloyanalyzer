package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;

/**
 * Created by marcel on 3/31/14.
 */
public class TypeExprVarVisit {
    public static String visit (ExprVar x, VisitorExprType visitor) throws Err {
        int depth = visitor.getDepth();
        String[] nameArr = x.label.split("/");
        if(nameArr.length <= 1) {
            return nameArr[0].replace("}","").replace("{","");
        }
        return nameArr[1].replace("}","").replace("{","");
    }
}
