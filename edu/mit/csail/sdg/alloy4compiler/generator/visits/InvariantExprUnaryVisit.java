package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorInvariant;

/**
 * Created by marcel on 3/23/14.
 */
public class InvariantExprUnaryVisit {
    public static String visit (ExprUnary x, VisitorInvariant visitor) throws Err {
        int depth = visitor.getDepth();

        StringBuilder res = new StringBuilder();
        switch (x.op) {
            // TODO: remove commented lines
            case SOMEOF:
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(" != null && ").append(visitor.currentLabel).append(".Count() > 0").append(");\n");
                res.append(x.sub.accept(visitor));
                break;
            case LONEOF:
                res.append(x.sub.accept(visitor));
                // nothing needed
                break;
            case ONEOF:
                // not null
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(" != null").append(");\n");
                res.append(x.sub.accept(visitor));
                break;
            case SETOF:
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(" != null").append(");\n");
                res.append(x.sub.accept(visitor));
                break;
            case EXACTLYOF:
                break;
            case NOT:
                break;
            case NO:
                break;
            case SOME:
                break;
            case LONE:
                break;
            case ONE:
                break;
            case TRANSPOSE:
                break;
            case RCLOSURE:
                break;
            case CLOSURE:
                break;
            case CARDINALITY:
                break;
            case CAST2INT:
                break;
            case CAST2SIGINT:
                break;
            case NOOP:
                res.append(x.sub.accept(visitor));
                break;
        }

        return new String(res);
    }
}
