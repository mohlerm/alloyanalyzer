package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;

/**
 * Created by marcel on 3/23/14.
 */
public class TypeExprListVisit {
    public static String visit (ExprList x, VisitorExprType visitor) throws Err {
        int depth = visitor.getDepth();

        StringBuilder res = new StringBuilder();

        switch (x.op) {


//            case DISJOINT:
//                break;
//            case TOTALORDER:
//                break;
            // TODO: implement TypeExprListVisit
            case AND:
                for (Expr e : x.args) {
                    res.append(e.accept(visitor));
                }
                break;
            case OR:
                for (Expr e : x.args) {
                    res.append(e.accept(visitor));
                }
                break;
        }
        return new String(res);
    }
}