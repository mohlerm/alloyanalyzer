package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorInvariant;

/**
 * Created by marcel on 3/23/14.
 */
public class InvariantExprVarVisit {
    public static String visit (ExprVar x, VisitorInvariant visitor) throws Err {
        int depth = visitor.getDepth();

        StringBuilder res = new StringBuilder(x.toString());

        return new String(res);
    }
}
