package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;

/**
 * Created by marcel on 3/31/14.
 */
public class TypeExprUnaryVisit {
    public static String visit (ExprUnary x, VisitorExprType visitor) throws Err {
        int depth = visitor.getDepth();
        StringBuilder res = new StringBuilder();

        switch(x.op) {

            case SOMEOF:
                if(depth <= 2) {
                res.append("ISet<").append(x.sub.accept(visitor)).append(">");
                } else {
                    res.append(x.sub.accept(visitor));
                }
                break;
            case LONEOF:
                res.append(x.sub.accept(visitor));
                break;
            case ONEOF:
                res.append(x.sub.accept(visitor));
                break;
            case SETOF:
                if(depth <= 2) {
                    res.append("ISet<").append(x.sub.accept(visitor)).append(">");
                } else {
                    res.append(x.sub.accept(visitor));
                }
                break;
//            case EXACTLYOF:
//                break;
            case NOT:
                break;
//            case NO:
//                break;
//            case SOME:
//                break;
//            case LONE:
//                break;
//            case ONE:
//                break;
            case TRANSPOSE:
                break;
            case RCLOSURE:
                break;
            case CLOSURE:
                break;
            case CARDINALITY:
                break;
            case CAST2INT:
                break;
            case CAST2SIGINT:
                break;
            case NOOP:
                res.append(x.sub.accept(visitor));
                break;
        }
        return new String(res);
    }
}
