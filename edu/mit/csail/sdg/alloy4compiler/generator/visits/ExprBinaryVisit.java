package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;

/**
 * Created by marcel on 3/23/14.
 */
@Deprecated
public class ExprBinaryVisit {
    public static String visit (ExprBinary expr, Visitor vis) throws Err {
        int depth = vis.getDepth();

        StringBuilder ans = new StringBuilder();
        ans.append("(");
        ans.append(expr.left.accept(vis)).append("$");
        ans.append(expr.op.toString()).append("$");
        ans.append(expr.right.accept(vis));
        ans.append(")");

        return new String(ans);
    }
}
