package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorInvariant;

/**
 * Created by marcel on 3/16/14.
 */
public class SigVisit {
    public static String visit (Sig sig, Visitor vis) throws Err {
        StringBuilder definitions = new StringBuilder();
        String offset = vis.getOffset();

        // if it's an "inner sig"
        if(vis.getDepth()>0) {
            String sigName = sig.label.split("/")[1];
            if (sig.isAbstract != null) {
                definitions.append("abstract ");
            }

            definitions.append("inner SIG ").append(sigName);
            if(sig.isSome != null) {
                definitions.append(" SOME");
            }
            else if(sig.isOne != null) {
                definitions.append(" ONE");
            }
            else if(sig.isLone != null) {
                definitions.append(" LONE");
            }

            definitions.append("\n");

            //System.out.println("DEBUG: " + sig.toString() + " Sig visited with depth " + sig.getDepth());
        }


        // if sig is an outer sig
        else {


            String currSigName = sig.label.split("/")[1];
            if (sig.isAbstract != null) {
                definitions.append("abstract ");
            }
            definitions.append("public class ").append(currSigName);

            if(sig.isSubsig != null) {
                Sig parent = ((Sig.PrimSig)sig).parent;
                if(parent!=Sig.UNIV) {
                    definitions.append(" : ").append(parent.label.split("/")[1]);
                }
            }

            definitions.append(" {\n");

            StringBuilder fieldDefinitions = new StringBuilder();
            StringBuilder fieldContracts = new StringBuilder();
            // get all fields
            for (Sig.Field s : sig.getFields()) {
                VisitorExprType visitorExprType = new VisitorExprType();

                if (s.isPrivate != null) {
                    fieldDefinitions.append("\tpublic ");
                } else {
                    fieldDefinitions.append("\tpublic ");
                }
                fieldDefinitions.append(s.accept(visitorExprType)).append(" ");
                fieldDefinitions.append(s.label).append(";\n");

            }
            definitions.append(fieldDefinitions);
            //if (fieldDefinitions.length() > 0 && fieldContracts.length() > 0) {
            // TODO: add invariants
            for (Sig.Field s : sig.getFields()) {
                VisitorInvariant visitorInvariant = new VisitorInvariant();
                fieldContracts.append(s.accept(visitorInvariant));
            }

            if (fieldContracts.length() > 0) {

                definitions.append("\n\t[ContractInvariantMethod]\n\tprivate void ObjectInvariant() {\n");
                definitions.append(fieldContracts);
                definitions.append("\t}\n");
            }

            //check for one
            if (sig.isOne != null) {
                definitions.append("\tprivate static ").append(currSigName).append(" instance;\n");
                definitions.append("\tprivate ").append(currSigName).append("() { }\n");
                definitions.append("\tpublic static ").append(currSigName).append(" Instance {\n");
                definitions.append("\t\tget {\n");
                definitions.append("\t\t\tif (instance == null) {\n\t\t\t\tinstance = new ").append(currSigName);
                definitions.append("();\n\t\t\t}\n\t\t\treturn instance;\n\t\t}\n\t}\n");
            }


            definitions.append("}\n\n");
        }

        return new String(definitions);
    }
}
