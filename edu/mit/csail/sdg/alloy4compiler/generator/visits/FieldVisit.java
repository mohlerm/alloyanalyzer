package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;

/**
 * Created by marcel on 3/16/14.
 */
@Deprecated
public class FieldVisit {
    public static String visit(Sig.Field field, Visitor vis) throws Err {
        int depth = vis.getDepth();
//        String offset = vis.getOffset();
//
//        String fieldName = field.label;
//
//        // definitions string contains field definitions, contracts string contains contracts (e.g. invariants)
       StringBuilder definitions = new StringBuilder();
        StringBuilder contracts = new StringBuilder();
//
//        // check for arity
//        String[] typeArr = field.type().toString().split("->");
//
//        if (depth <= 1) {
//            // next line is for debug purpose only
//            //definitions.append("//field ").append(fieldName).append(" from type ").append(field.type()).append("{\n");
//
//
//            // definitions setup with proper indentation
//            definitions.append(offset);
//            // if field private
//            if (field.isPrivate != null) {
//                definitions.append("private ");
//            }
//
//            // TODO implement proper subexpressions
//
//
//            // for binary and ternary relations
//
//            definitions.append("public ");
//            if (typeArr.length < 3) {
//                definitions.append(typeArr[1].split("/")[1].replace("}", ""));
//            }
//
//            contracts.append(offset);
//            if (field.oneOf() != null) {
//                contracts.append("\tContract.Invariant(").append(fieldName).append(" != null);\n");
//            }
//            //else if(field.someOf() != null) {
//            //    definitions.append(" SOME");
//            //}
//            // TODO: implement lone
//            else if (field.loneOf() != null) {
//                definitions.append(" LONE");
//            }
//
//            // if ternary relation
//            if (typeArr.length == 3) {
//                // figure out the sub expression names with the visitor
//
//                //definitions.append(offset).append(field.decl().expr.accept(vis));
//                definitions.append("ISet<Tuple<").append(typeArr[1].split("/")[1].replace("}", "")).append(", ");
//                definitions.append(typeArr[2].split("/")[1].replace("}", "")).append(">>");
//                // also add new invariant
//                //System.out.println("DEBUG NAME: "+ field.toString());
//
//                // TODO fix ternary invariants
//                VisitorObj visObj = new VisitorObj();
//
//                ExprObj exprObj = field.decl().expr.accept(visObj);
//
//                ExprBinary.Op operator =  null;
//                if(exprObj.operator != null) {
//                    operator = exprObj.operator;
//                }
//                String leftType = null;
//                String leftName = null;
//                String rightType = null;
//                String rightName = null;
//                // if relation
//
//                switch(operator) {
//                    case ARROW:
//                        // TODO needs to evaluate the complete tree (use while: nodes != null)
//                        //((null$.$Platform#floor)$->$(null$.$Platform#ceiling))
//                        leftType = exprObj.left.right.right.type; // in that case platform
//                        leftName = exprObj.left.right.right.name; // in that case floor
//                        // left.left is null with operand "."
//                        rightType = exprObj.right.right.right.type; // in that case platform
//                        rightName = exprObj.right.right.right.name; // in that case ceiling
//                        // right.left is null with operand "."
//
//
//                        contracts.append("\t\tContract.Invariant(").append(fieldName).append(" != null && ");
//                        contracts.append(leftName).append(" != null && Contract.ForAll(").append(fieldName);
//                        contracts.append(", e1 => ").append(leftName).append(".Equals(e1.Item1)));\n");
//
//                        contracts.append("\t\tContract.Invariant(").append(fieldName).append(" != null && ");
//                        contracts.append(rightName).append(" != null && Contract.ForAll(").append(fieldName);
//                        contracts.append(", e1 => ").append(rightName).append(".Equals(e1.Item2)));\n");
//                        break;
//                    case ANY_ARROW_SOME:
//                        break;
//                    case ANY_ARROW_ONE:
//                        break;
//                    case ANY_ARROW_LONE:
//                        break;
//                    case SOME_ARROW_ANY:
//                        break;
//                    case SOME_ARROW_SOME:
//                        break;
//                    case SOME_ARROW_ONE:
//                        break;
//                    case SOME_ARROW_LONE:
//                        break;
//                    case ONE_ARROW_ANY:
//                        break;
//                    case ONE_ARROW_SOME:
//                        break;
//                    case ONE_ARROW_ONE:
//                        break;
//                    case ONE_ARROW_LONE:
//                        break;
//                    case LONE_ARROW_ANY:
//                        break;
//                    case LONE_ARROW_SOME:
//                        break;
//                    case LONE_ARROW_ONE:
//                        break;
//                    case LONE_ARROW_LONE:
//                        break;
//                    case ISSEQ_ARROW_LONE:
//                        break;
//                    case JOIN:
//                        break;
//                    case DOMAIN:
//                        break;
//                    case RANGE:
//                        break;
//                    case INTERSECT:
//                        break;
//                    case PLUSPLUS:
//                        break;
//                    case PLUS:
//                        break;
//                    case IPLUS:
//                        break;
//                    case MINUS:
//                        break;
//                    case IMINUS:
//                        break;
//                    case MUL:
//                        break;
//                    case DIV:
//                        break;
//                    case REM:
//                        break;
//                    case EQUALS:
//                        break;
//                    case NOT_EQUALS:
//                        break;
//                    case IMPLIES:
//                        break;
//                    case LT:
//                        break;
//                    case LTE:
//                        break;
//                    case GT:
//                        break;
//                    case GTE:
//                        break;
//                    case NOT_LT:
//                        break;
//                    case NOT_LTE:
//                        break;
//                    case NOT_GT:
//                        break;
//                    case NOT_GTE:
//                        break;
//                    case SHL:
//                        break;
//                    case SHA:
//                        break;
//                    case SHR:
//                        break;
//                    case IN:
//                        break;
//                    case NOT_IN:
//                        break;
//                    case AND:
//                        break;
//                    case OR:
//                        break;
//                    case IFF:
//                        break;
//                }
//
//
//            }
//
//            definitions.append(" ").append(fieldName).append(";\n");
//        }

        // if it's deep in the tree, just hand over type and name
//        else {
//            // TODO: remove that debug statement (and the whole branch is kinda useless)
//            System.out.println("ERROR: SHOULD NOT HAPPEN ANYMORE");
////            definitions.append(typeArr[1].split("/")[1].replace("}", ""));
////            contracts.append(fieldName);
//        }


        return (new String(definitions) + "#" + new String(contracts));
    }
}


