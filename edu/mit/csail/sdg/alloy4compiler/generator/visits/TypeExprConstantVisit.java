package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;

/**
 * Created by marcel on 3/23/14.
 */
public class TypeExprConstantVisit {
    public static String visit (ExprConstant x, VisitorExprType visitor) throws Err {
        int depth = visitor.getDepth();

        StringBuilder res = new StringBuilder();

        switch (x.op) {


            case TRUE:
                res.append(x.accept(visitor));
                break;
            case FALSE:
                break;
            case IDEN:
                break;
            case MIN:
                break;
            case MAX:
                break;
            case NEXT:
                break;
            case EMPTYNESS:
                break;
            case STRING:
                break;
            case NUMBER:
                break;
        }
        return new String(res);
    }
}