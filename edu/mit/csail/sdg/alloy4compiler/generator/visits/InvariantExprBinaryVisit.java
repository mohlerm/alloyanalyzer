package edu.mit.csail.sdg.alloy4compiler.generator.visits;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.generator.*;

/**
 * Created by marcel on 3/23/14.
 */
public class InvariantExprBinaryVisit {
    public static String visit (ExprBinary x, VisitorInvariant visitor) throws Err {
        int depth = visitor.getDepth();

        StringBuilder res = new StringBuilder("");
        VisitorMultiplicity visMult = new VisitorMultiplicity();
        VisitorField visitorField = new VisitorField();
        VisitorSig visitorSig = new VisitorSig();

        String leftMult = (x.left.accept(visMult));
        Boolean leftIsField = (x.left.accept(visitorField));
        String rightMult = (x.right.accept(visMult));
        Boolean rightIsField = (x.right.accept(visitorField));

        switch (x.op) {

            case ARROW:
                break;
            case ANY_ARROW_SOME:
                rightMult = "some";
                break;
            case ANY_ARROW_ONE:
                rightMult = "one";
                break;
            case ANY_ARROW_LONE:
                rightMult = "lone";
                break;
            case SOME_ARROW_ANY:
                leftMult = "some";
                break;
            case SOME_ARROW_SOME:
                leftMult = "some";
                rightMult = "some";
                break;
            case SOME_ARROW_ONE:
                leftMult = "some";
                rightMult = "one";
                break;
            case SOME_ARROW_LONE:
                leftMult = "some";
                rightMult = "lone";
                break;
            case ONE_ARROW_ANY:
                leftMult = "one";
                break;
            case ONE_ARROW_SOME:
                leftMult = "one";
                rightMult = "some";
                break;
            case ONE_ARROW_ONE:
                leftMult = "one";
                rightMult = "one";
                break;
            case ONE_ARROW_LONE:
                leftMult = "one";
                rightMult = "lone";
                break;
            case LONE_ARROW_ANY:
                leftMult = "lone";
                break;
            case LONE_ARROW_SOME:
                leftMult = "lone";
                rightMult = "some";
                break;
            case LONE_ARROW_ONE:
                leftMult = "lone";
                rightMult = "one";
                break;
            case LONE_ARROW_LONE:
                leftMult = "lone";
                rightMult = "lone";
                break;
//            case ISSEQ_ARROW_LONE:
//                break;
            case JOIN:
//                //TODO: wtf
//                res.append(visitor.rootLabel).append(" != null && ").append(x.right.accept(visitor));
                break;
            case DOMAIN:
                break;
            case RANGE:
                break;
            case INTERSECT:
                return ("\t\tContract.Invariant(" + visitor.rootLabel + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " && " + visitor.rootLabel + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");

            case PLUSPLUS:
                break;
            case PLUS:
                return ("\t\tContract.Invariant(" + visitor.rootLabel + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " || " + visitor.rootLabel + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");

            case IPLUS:
                break;
            case MINUS:
                return ("\t\tContract.Invariant(" + visitor.rootLabel + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " || " + visitor.rootLabel + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");

            case IMINUS:
                break;
            case MUL:
                break;
            case DIV:
                break;
            case REM:
                break;
            case EQUALS:
                break;
            case NOT_EQUALS:
                break;
            case IMPLIES:
                break;
            case LT:
                break;
            case LTE:
                break;
            case GT:
                break;
            case GTE:
                break;
            case NOT_LT:
                break;
            case NOT_LTE:
                break;
            case NOT_GT:
                break;
            case NOT_GTE:
                break;
            case SHL:
                break;
            case SHA:
                break;
            case SHR:
                break;
            case IN:
                break;
            case NOT_IN:
                break;
            case AND:
                break;
            case OR:
                break;
            case IFF:
                break;
        }

        if(visitor.currentLabel == null)
        {
            visitor.currentLabel = "NAMEERROR";
        }


        //if(leftMult != null || rightMult!=null) {
        res.append("\t\tContract.Invariant(").append(visitor.rootLabel).append(" != null").append(");\n");
//        } else {
//            res.append(visitor.rootLabel + " != null");
//        }


        //res.append(visitor.rootLabel + " != null");
        if(leftIsField != null && leftIsField && leftMult != null) {
            if( Debug.enableInvariantOutput) System.out.println("MULT: LEFT: " + leftMult);
            x.left.accept(visitor);
            if (leftMult.equals("one")) {
                res.append("\t\tContract.Invariant(").append("Contract.ForAll(").append(visitor.rootLabel).append(", e1 => e1 != null && ").append(visitor.currentLabel).append(".Equals(e1.Item1))").append(");\n");
            } else if (leftMult.equals("lone")) {
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(".Count() <= 1").append(");\n");
            } else if (leftMult.equals("some")) {
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(".Count() > 0").append(");\n  ");
//            } else if (leftMult.equals("set")) {
//                //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item1 != null));\n");
            }
        } else if(leftIsField != null && !leftIsField && leftMult != null) {

            if (leftMult.equals("one")) {
                res.append("\t\tContract.Invariant(" + visitor.rootLabel + ".Count() == 0 || " + "Contract.Exists(" + visitor.rootLabel + ", r => Contract.ForAll(" + visitor.rootLabel + ", s => s.Item1.Equals(r.Item1))));\n");
            } else if (leftMult.equals("lone")) {
                res.append("\t\tContract.Invariant(" + visitor.rootLabel + ".Count() == 0 || " + "Contract.Exists(" + visitor.rootLabel + ", r => Contract.ForAll(" + visitor.rootLabel + ", s => s.Item1.Equals(r.Item1))));\n");
            } else if (leftMult.equals("some")) {
                res.append("\t\tContract.Invariant(" + "Contract.ForAll(" + visitor.rootLabel + ", m => m.Item1 != null));\n");
//            } else if (leftMult.equals("set")) {
//                //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item1 != null));\n");
            }
        }

        if(rightIsField != null && rightIsField && rightMult != null) {
            x.right.accept(visitor);
            if( Debug.enableInvariantOutput) System.out.println("MULT: RIGHT: " + rightMult);
            if (rightMult.equals("one")) {
                res.append("\t\tContract.Invariant(").append("Contract.ForAll(").append(visitor.rootLabel).append(", e1 => e1 != null && ").append(visitor.currentLabel).append(".Equals(e1.Item2))").append(");\n");
            } else if (rightMult.equals("lone")) {
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(".Count() <= 1").append(");\n");
            } else if (rightMult.equals("some")) {
                res.append("\t\tContract.Invariant(").append(visitor.currentLabel).append(".Count() > 0").append(");\n");
//            } else if (rightMult.equals("set")) {
//                //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item2 != null));\n");
            }
        } else if(rightIsField != null && !rightIsField && rightMult != null) {

            if (rightMult.equals("one")) {
                res.append("\t\tContract.Invariant(" + visitor.rootLabel + ".Count() == 0 || " + "Contract.Exists(" + visitor.rootLabel + ", r => Contract.ForAll(" + visitor.rootLabel + ", s => s.Item2.Equals(r.Item2)))").append(");\n");
            } else if (rightMult.equals("lone")) {
                res.append("\t\tContract.Invariant(" + visitor.rootLabel + ".Count() == 0 || " + "Contract.Exists(" + visitor.rootLabel + ", r => Contract.ForAll(" + visitor.rootLabel + ", s => s.Item2.Equals(r.Item2)))").append(");\n");
            } else if (rightMult.equals("some")) {
                res.append("\t\tContract.Invariant(" + "Contract.ForAll(" + visitor.rootLabel + ", m => m.Item2 != null)").append(");\n");
//            } else if (rightMult.equals("set")) {
//                //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item2 != null));\n");
            }

//        } else {
//            res.append("\t\tContract.Invariant(true");
        }


//        } else {
//            res.append(visitor.rootLabel + " != null");
        return new String(res);
    }
}
