package edu.mit.csail.sdg.alloy4compiler.generator.codegens;

import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Decl;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprHasName;
import edu.mit.csail.sdg.alloy4compiler.ast.Func;
import edu.mit.csail.sdg.alloy4compiler.generator.*;

import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by marcel on 3/30/14.
 */
public class FuncCodeGen implements ICodeGen{
    private String _originalFilename;
    private PrintWriter _out;
    private ConstList<Func> _funcList;
    private int _listSize;

    public FuncCodeGen(String originalFilename, PrintWriter out, Iterable<Func> funcs) {
        _originalFilename = originalFilename;
        _out = out;
        _funcList = ConstList.make(funcs);
        _listSize = _funcList.size();
    }
    public void generate() {
        StringBuilder str = new StringBuilder();
        str.append("public static class FuncClass {\n");
        for (Func currFunc:_funcList) {
            try {
                // private
                if (currFunc.isPrivate != null) {
                    str.append("\tpublic ");
                }
                // public
                else {
                    str.append("\tpublic ");
                }
                str.append("static ");

                VisitorExprType visitorExprType = new VisitorExprType();
                VisitorSet visitorSet = new VisitorSet();
                String returnInvariant = "";
                // if func is a predicate
                if (currFunc.isPred) {
                    str.append("bool ");
                } else {
                    String returnString = currFunc.returnDecl.accept(visitorExprType);
                    str.append(returnString).append(" ");
                    Boolean isSet;
                    isSet = currFunc.returnDecl.accept(visitorSet);
                    VisitorConditions visitorConditions = new VisitorConditions(returnString, true, isSet);
                    returnInvariant = currFunc.returnDecl.accept(visitorConditions);
                    //}
                }
                // append the func name
                str.append(currFunc.label.split("/")[1]).append("(");

                // print the parameters
                int j = 1;

                String temp = "";
                for (Decl dec:currFunc.decls) {
                    for(ExprHasName ex : dec.names) {
                        temp += (dec.expr.accept(visitorExprType)) + (" ") + (ex) + (", ");
                    }
                }
                // kinda ugly
                if(temp.length()>2) {
                    str.append(temp.substring(0, temp.length() - 2));
                }


                str.append(") {\n");

                // add invariants
                //VisitorInvariant visitorInvariant = new VisitorInvariant();
                HashMap<String, Boolean> mapSets = new HashMap<String, Boolean>();

                // add preconditions
                temp = "";
                for(Decl dec:currFunc.decls) {
                    for(ExprHasName ex : dec.names) {
                        Boolean isSet;
                        isSet = dec.expr.accept(visitorSet);
                        VisitorConditions visitorConditions= new VisitorConditions(ex.toString(), false, isSet);

                        temp = dec.expr.accept(visitorConditions);
                        mapSets.put(ex.label, dec.expr.accept(visitorSet));

//                        if(!invariant.isEmpty()) {
//                            // TODO: very ugly since the invariant contains "null"
//                            temp += ("\t\tContract.Requires(") + (ex) + (invariant.substring(4)) + (");\n");
//                            temp = temp.replace("null.Count()",ex+".Count()");
//
//                        }
//                        if(dec.expr.accept(visitorSet).equals(Boolean.TRUE)) {
//                            temp += ("\t\tContract.Requires(")  + (ex) + (" != null);\n");
//                        }
                    }
                }
                if(!temp.isEmpty()) {
                    str.append(temp);
                }
                if(!returnInvariant.isEmpty()) {
                    str.append(returnInvariant);
                }
                str.append("\t\treturn ");
                VisitorFunc visitorFunc = new VisitorFunc(mapSets);
                visitorFunc.zeroingDepth();
                str.append(currFunc.getBody().accept(visitorFunc));

                str.append(";\n\t}\n");

            } catch(Err err){
                err.printStackTrace();
            }


        }
        _out.print(str);
        _out.println("}\n");
    }
}
