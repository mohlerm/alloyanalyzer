package edu.mit.csail.sdg.alloy4compiler.generator.codegens;

/**
 * Created by marcel on 3/14/14.
 */
public interface ICodeGen {
    abstract public void generate();
}
