package edu.mit.csail.sdg.alloy4compiler.generator.codegens;

import java.io.PrintWriter;

/**
 * Created by marcel on 3/30/14.
 */
public class HelperCodeGen implements ICodeGen {
    private String _originalFilename;
    private PrintWriter _out;

    public HelperCodeGen(String originalFilename, PrintWriter out) {
        _originalFilename = originalFilename;
        _out = out;
    }
    public static boolean closureNeeded = false;
    public static boolean rclosureNeeded = false;
    public static boolean transposeNeeded = false;
    public static boolean intersectNeeded = false;
    public static boolean unionNeeded = false;
    public static boolean exceptNeeded = false;

    public void generate() {
        if(closureNeeded || rclosureNeeded || transposeNeeded || intersectNeeded || unionNeeded || exceptNeeded) {
        _out.println("public static class Helper {");
        }
        if(closureNeeded || rclosureNeeded) {
        _out.println("\tpublic static ISet<Tuple<L, R>> Closure<L, R>(ISet<Tuple<L, R>> set) {\n" +
                "\t\tISet<Tuple<L, R>> res = set;\n" +
                "\t\tISet<Tuple<L, R>> old = new HashSet<Tuple<L, R>>();\n" +
                "\t\tISet<Tuple<L, R>> neu = new HashSet<Tuple<L, R>>(res);\n" +
                "\t\twhile(!neu.SetEquals(old)) {\n" +
                "\t\t\told = new HashSet<Tuple<L, R>>(res);\n" +
                "\t\t\tres = new HashSet<Tuple<L, R>>(neu);\n" +
                "\t\t\tforeach(Tuple<L,R> s1 in res) {\n" +
                "\t\t\t\tforeach(Tuple<L,R> s2 in res) {\n" +
                "\t\t\t\t\tif((s1.Item2.Equals(s2.Item1))) {\n" +
                "\t\t\t\t\t\tTuple<L, R> n = new Tuple<L, R>(s1.Item1, s2.Item2);\n" +
                "\t\t\t\t\t\tif(!(res.Contains(n))) {\n" +
                "\t\t\t\t\t\t\tneu.Add(n);\n" +
                "\t\t\t\t\t\t}\n" +
                "\t\t\t\t\t}\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}\n");
        }
        if(rclosureNeeded) {
        _out.println("\tpublic static ISet<Tuple<L, R>> RClosure<L, R>(ISet<Tuple<L, R>> set) { \n" +
                "\t\tset = Helper.Closure<L, R>(set);\n" +
                "\t\tISet<Tuple<L, R>> res = new HashSet<Tuple<L, R>>(set);\n" +
                "\t\tforeach (Tuple<L, R> s in set) {\n" +
                "\t\t\tObject temp = (Object)s.Item1;\n" +
                "\t\t\tR itm1 = (R)temp;\n" +
                "\t\t\ttemp = (Object)s.Item2;\n" +
                "\t\t\tL itm2 = (L)temp;\n" +
                "\t\t\tTuple<L, R> n1 = new Tuple<L, R>(s.Item1, itm1);\n" +
                "\t\t\tTuple<L, R> n2 = new Tuple<L, R>(itm2, s.Item2);\n" +
                "\t\t\tif (!(res.Contains(n1))) {\n" +
                "\t\t\t\tres.Add(n1);\n" +
                "\t\t\t} else if (!(res.Contains(n2))) {\n" +
                "\t\t\t\tres.Add(n2);\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}\n");
        }
        if(transposeNeeded) {

        _out.println("\tpublic static ISet<Tuple<L, R>> Transpose<L, R>(ISet<Tuple<L, R>> set) {\n" +
                "\t\tISet<Tuple<L, R>> res = new HashSet<Tuple<L, R>>();\n" +
                "\t\tforeach (Tuple<L, R> s in set) {    \n" +
                "\t\t\tObject temp = (Object)s.Item1;\n" +
                "\t\t\tR itm1 = (R)temp;\n" +
                "\t\t\ttemp = (Object)s.Item2;\n" +
                "\t\t\tL itm2 = (L)temp;\n" +
                "\t\t\tres.Add(new Tuple<L, R>(itm2, itm1));\n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}");
        }
        if(intersectNeeded || exceptNeeded) {
        _out.println("\tpublic static ISet<Object> Intersect<Object>(ISet<Object> setA, ISet<Object> setB) {\n" +
                "\t\tISet<Object> res = new HashSet<Object>();\n" +
                "\t\tforeach (Object o1 in setA) {\n" +
                "\t\t\tif(setB.Contains(o1)) {\n" +
                "\t\t\t\tres.Add(o1);\n" +
                "\t\t\t}   \n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}");
        }
        if(unionNeeded) {
        _out.println("\tpublic static ISet<Object> Union<Object>(ISet<Object> setA, ISet<Object> setB) {\n" +
                "\t\tISet<Object> res = new HashSet<Object>();\n" +
                "\t\tforeach (Object o1 in setA) {\n" +
                "\t\t\tres.Add(o1);\n" +
                "\t\t}\n" +
                "\t\tforeach (Object o2 in setB) {\n" +
                "\t\t\tres.Add(o2);\n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}");
        }
        if(exceptNeeded) {
        _out.println("\tpublic static ISet<Object> Except<Object>(ISet<Object> setA, ISet<Object> setB) {\n" +
                "\t\tISet<Object> res = new HashSet<Object>(setA);\n" +
                "\t\tforeach (Object o in Helper.Intersect<Object>(setA, setB)) {\n" +
                "\t\t\tres.Remove(o);\n" +
                "\t\t}\n" +
                "\t\treturn res;\n" +
                "\t}");
        }
        if(closureNeeded || rclosureNeeded || transposeNeeded || intersectNeeded || unionNeeded || exceptNeeded) {
        _out.println("}");
        }
    }
}