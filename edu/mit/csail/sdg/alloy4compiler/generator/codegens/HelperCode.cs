using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlloyAnalyzerHelper
{
    class Helper
    {
        static void Main(string[] args)
        {
            
            ISet<UInt32> test1 = new HashSet<UInt32>();
            ISet<UInt32> test2 = new HashSet<UInt32>();
            ISet<UInt32> res = new HashSet<UInt32>();
            test1.Add(8);
            test1.Add(5);
            test1.Add(2);
            test1.Add(9);
            test1.Add(2);
            test1.Add(9);
            test1.Add(2);
            test1.Add(1);
            test2.Add(8);
            test2.Add(5);
            test2.Add(0);
            test2.Add(0);
            test2.Add(1);
            test2.Add(0);
            test2.Add(2);
            test2.Add(1);

            res = Except<UInt32>(test1, test2);
            int i = 1;
            foreach (UInt32 s in res) {
                System.Console.WriteLine("Item " + i + ":\t\t" + s.ToString());
                i++;
            }
            System.Console.ReadKey();
        }
        public static ISet<Tuple<L, R>> Closure<L, R>(ISet<Tuple<L, R>> set) {
            ISet<Tuple<L, R>> res = set;
            ISet<Tuple<L, R>> old = new HashSet<Tuple<L, R>>();
            ISet<Tuple<L, R>> neu = new HashSet<Tuple<L, R>>(res);
            while(!neu.SetEquals(old)) {
                old = new HashSet<Tuple<L, R>>(res);
                res = new HashSet<Tuple<L, R>>(neu);
                foreach(Tuple<L,R> s1 in res) {
                    foreach(Tuple<L,R> s2 in res) {
                        if((s1.Item2.Equals(s2.Item1))) {
                            Tuple<L, R> n = new Tuple<L, R>(s1.Item1, s2.Item2);
                            if(!(res.Contains(n))) {
                                neu.Add(n);
                            }
                        }
                    }
                }   
            }
            return res;
        }
        public static ISet<Tuple<L, R>> RClosure<L, R>(ISet<Tuple<L, R>> set)
        {
            set = Helper.Closure<L, R>(set);
            ISet<Tuple<L, R>> res = new HashSet<Tuple<L, R>>(set);
            foreach (Tuple<L, R> s in set) {
                Object temp = (Object)s.Item1;
                R itm1 = (R)temp;
                temp = (Object)s.Item2;
                L itm2 = (L)temp;
                Tuple<L, R> n1 = new Tuple<L, R>(s.Item1, itm1);
                Tuple<L, R> n2 = new Tuple<L, R>(itm2, s.Item2);
                if (!(res.Contains(n1))) {
                    res.Add(n1);
                }
                else if (!(res.Contains(n2))) {
                    res.Add(n2);
                }
            }
            return res;
        }
        public static ISet<Tuple<L, R>> Transpose<L, R>(ISet<Tuple<L, R>> set) {
            ISet<Tuple<L, R>> res = new HashSet<Tuple<L, R>>();
            foreach (Tuple<L, R> s in set) {    
                Object temp = (Object)s.Item1;
                R itm1 = (R)temp;
                temp = (Object)s.Item2;
                L itm2 = (L)temp;
                res.Add(new Tuple<L, R>(itm2, itm1));
            }
            return res;
        }
        public static ISet<Object> Intersect<Object>(ISet<Object> setA, ISet<Object> setB) {
            ISet<Object> res = new HashSet<Object>();
            foreach (Object o1 in setA) {
                if(setB.Contains(o1)) {
                    res.Add(o1);
                }   
            }
            return res;
        }
        public static ISet<Object> Union<Object>(ISet<Object> setA, ISet<Object> setB) {
            ISet<Object> res = new HashSet<Object>();
            foreach (Object o1 in setA) {
                res.Add(o1);
            }
            foreach (Object o2 in setB) {
                res.Add(o2);
            }
            return res;
        }
        public static ISet<Object> Except<Object>(ISet<Object> setA, ISet<Object> setB) {
            ISet<Object> res = new HashSet<Object>(setA);
            foreach (Object o in Helper.Intersect<Object>(setA, setB)) {
                res.Remove(o);
            }
            return res;
        }
    }
}
