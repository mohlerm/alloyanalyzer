package edu.mit.csail.sdg.alloy4compiler.generator.codegens;

import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.generator.Visitor;

import java.io.PrintWriter;

/**
 * Created by marcel on 3/12/14.
 */
public class SigCodeGen implements ICodeGen{
    private String _originalFilename;
    private PrintWriter _out;
    private ConstList<Sig> _sigList;
    private int _listSize;

    public SigCodeGen(String originalFilename, PrintWriter out, Iterable<Sig> sigs) {
        _originalFilename = originalFilename;
        _out = out;
        _sigList = ConstList.make(sigs);
        _listSize = _sigList.size();
    }

    public void generate() {
        // next line is for debug purpose only
        //_out.println("//Amount of Sigs: " + Integer.toString(_listSize) + " (6 are built-in)");
        Visitor visitor = new Visitor();
        for (Sig currSig:_sigList) {
            if(!currSig.builtin) {
                try {
                    visitor.zeroingDepth();
                    _out.print(currSig.accept(visitor));
                } catch (Err err) {
                    err.printStackTrace();
                }
            }
        }
    }
}
