package edu.mit.csail.sdg.alloy4compiler.generator.codegens;

import java.io.PrintWriter;

/**
 * Created by marcel on 3/12/14.
 */
public class MainCodeGen implements ICodeGen{

    private String _originalFilename;
    private PrintWriter _out;
    private boolean _checkContracts;

    public MainCodeGen(String originalFilename, PrintWriter out, boolean checkContracts) {
        _originalFilename = originalFilename;
        _out = out;
        _checkContracts = checkContracts;
    }
    public void generate() {
        _out.println("// This C# file is generated from "+ _originalFilename);
        if(!_checkContracts) {
            _out.println("");
            _out.println("#undef CONTRACTS_FULL");
        }
        _out.println("");
        // TODO add needed imports
        _out.println("using System;");
        _out.println("using System.Linq;");
        _out.println("using System.Collections.Generic;");
        _out.println("using System.Diagnostics.Contracts;\n");
//        _out.println("public class " + className);
//        _out.println("{");
//        _out.println("\tpublic static void Main()");
//        _out.println("\t{");
    }

//    public void generateEnd() {
//        _out.println("\t}");
//        _out.println("}");
//    }
}
