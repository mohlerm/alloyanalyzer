package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.visits.*;

/**
 * Created by marcel on 3/31/14.
 */
public class VisitorExprType extends VisitQuery<String> {

    private int _currDepth;
    final boolean DEBUG = Debug.enableExprTypeOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }

    @Override public String visit(Sig.Field field) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ field.toString() + " Sig.Field visited with depth " + getDepth());

        String res = field.decl().expr.accept(this);


        decDepth();
        return res;
    }
    /** Visits an ExprVar node */
    @Override public String visit(ExprVar x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprVar visited with depth " + getDepth());
        String res = TypeExprVarVisit.visit(x, this);
        decDepth();
        return res;
    }
    /** Visits an ExprUnary node */
    @Override public String visit(ExprUnary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprUnary visited with depth " + getDepth());
        String res = TypeExprUnaryVisit.visit(x, this);
        decDepth();
        return res;
    }

    @Override public String visit(Sig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ sig.toString() + " Sig visited with depth " + getDepth());
        String[] nameArr = sig.label.split("/");
        if(nameArr.length <= 1) {
            return nameArr[0].replace("}","").replace("{","");
        }
        String res = nameArr[1].replace("}","").replace("{","");
        decDepth();
        return res;
    }

    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */
    @Override public String visit(ExprBinary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprBinary visited with depth " + getDepth());
        String res = TypeExprBinaryVisit.visit(x, this);
        decDepth();
        return res;
    }

    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    // no need to support DISJOINT, TOTALORDER
    @Override public String visit(ExprList x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprList visited with depth " + getDepth());
        String res = TypeExprListVisit.visit(x, this);
        decDepth();
        return res;
    }

    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprCall x) throws Err {
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprCall visited with depth " + getDepth());
        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        return null;
    }

    /** Visits an ExprConstant node (this default implementation simply returns null) */
    // no need to support IDEN, NEXT and EMPTYNESS
    @Override public String visit(ExprConstant x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprConstant visited with depth " + getDepth());
        String res = TypeExprConstantVisit.visit(x, this);
        decDepth();
        return res;

    }

    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */
    @Override public String visit(ExprITE x) throws Err {
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprITE visited with depth " + getDepth());
        String ans = x.cond.accept(this);
        if (ans==null) ans = x.left.accept(this);
        if (ans==null) ans = x.right.accept(this);
        return ans;
    }

    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
    @Override public String visit(ExprLet x) throws Err {
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprLet visited with depth " + getDepth());
        String ans = x.var.accept(this);
        if (ans==null) ans = x.expr.accept(this);
        if (ans==null) ans = x.sub.accept(this);
        return ans;
    }

    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
    @Override public String visit(ExprQt x) throws Err {
        if(DEBUG) System.out.println("DEBUG(ExprType): "+ x.toString() + " ExprQt visited with depth " + getDepth());
        for(Decl d: x.decls) {
            for(ExprHasName v: d.names) { String ans = v.accept(this); if (ans!=null) return ans; }
            String ans = d.expr.accept(this); if (ans!=null) return ans;
        }
        return x.sub.accept(this);
    }
}
