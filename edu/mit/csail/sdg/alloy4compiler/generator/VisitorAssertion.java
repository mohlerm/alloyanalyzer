package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

import java.util.Iterator;


/**
 * Created by Fabian on 31.03.14.
 */
public class VisitorAssertion extends VisitQuery<String> {

Boolean DEBUG = Debug.enableAssertionOutput;

VisitorExprType _typeVisitor = new VisitorExprType();
VisitorFunc _funcVisitor = new VisitorFunc();


    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */ // copied from Func Vis
    @Override public String visit(ExprBinary x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprBinary visited" + x.toString());

        StringBuilder str = new StringBuilder("");
        String left = x.left.accept(this);
        String right = x.right.accept(this);

        VisitorSet visitorSet = new VisitorSet();
        Boolean leftIsSet;
        Boolean rightIsSet;

        if (x.op.equals(ExprBinary.Op.IMPLIES) || x.op.equals(ExprBinary.Op.NOT_IN)){ // implies <=> !(A) | (B)
            str.append("!");
        }

        if(!(x.op.equals(ExprBinary.Op.JOIN))) {
            str.append("(");
        }
        //str.append(left);
        //String operator = "ERROR";
        switch (x.op) {
            case ARROW:
                break;
            case ANY_ARROW_SOME:
                break;
            case ANY_ARROW_ONE:
                break;
            case ANY_ARROW_LONE:
                break;
            case SOME_ARROW_ANY:
                break;
            case SOME_ARROW_SOME:
                break;
            case SOME_ARROW_ONE:
                break;
            case SOME_ARROW_LONE:
                break;
            case ONE_ARROW_ANY:
                break;
            case ONE_ARROW_SOME:
                break;
            case ONE_ARROW_ONE:
                break;
            case ONE_ARROW_LONE:
                break;
            case LONE_ARROW_ANY:
                break;
            case LONE_ARROW_SOME:
                break;
            case LONE_ARROW_ONE:
                break;
            case LONE_ARROW_LONE:
                break;
            case ISSEQ_ARROW_LONE:
                break;
            case JOIN:
                str.append(left).append(".").append(right) ;
                break;
            case DOMAIN:
                break;
            case RANGE:
                break;
            case INTERSECT:
                leftIsSet = x.left.accept(visitorSet);
                rightIsSet = x.right.accept(visitorSet);
                if(leftIsSet.equals(Boolean.TRUE)) {
                    str.append(left).append(").Intersect(").append(right);
                } else {
                    if (rightIsSet.equals(Boolean.TRUE)) {
                        str.append(right).append(").Intersect(").append(left);
                    } else {
                        str.append(left).append(") && (").append(right);
                    }
                }

                break;
            case PLUSPLUS:
                str.append(left).append(")++(").append(right) ;
                break;
            case PLUS:
                leftIsSet = x.left.accept(visitorSet);
                rightIsSet = x.right.accept(visitorSet);
                if(leftIsSet.equals(Boolean.TRUE)) {
                    str.append(left).append(").Union(").append(right);
                } else {
                    if (rightIsSet.equals(Boolean.TRUE)) {
                        str.append(right).append(").Union(").append(left);
                    } else {
                        str.append(left).append(") || (").append(right);
                    }
                }
                break;
            case IPLUS:
                str.append(left).append(").Integer.Plus(").append(right) ;
                break;
            case MINUS:
                leftIsSet = x.left.accept(visitorSet);
                rightIsSet = x.right.accept(visitorSet);
                if(leftIsSet.equals(Boolean.TRUE)) {
                    str.append(left).append(").Except(").append(right);
                } else {
                    if (rightIsSet.equals(Boolean.TRUE)) {
                        str.append(right).append(").Except(").append(left);
                    } else {
                        str.append(left).append(") - (").append(right);
                    }
                }
                break;
            case IMINUS:
                str.append(left).append(").Integer.Plus(").append(right);
                break;
            case MUL:
                str.append(left).append(")*(").append(right);
                break;
            case DIV:
                str.append(left).append(")/(").append(right);
                break;
            case REM:
                str.append(left).append(")%(").append(right);
                break;
            case EQUALS:
                str.append(left).append(").Equals(").append(right);
                break;
            case NOT_EQUALS:
                str.append(left).append(")!=(").append(right) ;
                break;
            case IMPLIES:
                str.append(left).append(") || (").append(right) ;
                break;
            case LT:
                str.append(left).append(")<(").append(right) ;
                break;
            case LTE:
                str.append(left).append(")<=(").append(right) ;
                break;
            case GT:
                str.append(left).append(")>(").append(right) ;
                break;
            case GTE:
                str.append(left).append(")>=(").append(right) ;
                break;
            case NOT_LT:
                str.append(left).append(")!<(").append(right) ;
                break;
            case NOT_LTE:
                str.append(left).append(")!<=(").append(right) ;
                break;
            case NOT_GT:
                str.append(left).append(")!>(").append(right) ;
                break;
            case NOT_GTE:
                str.append(left).append(")!>=(").append(right) ;
                break;
            case SHL:
                str.append(left).append(")<<(").append(right) ;
                break;
            case SHA:
                str.append(left).append(")>>(").append(right) ;
                break;
            case SHR:
                str.append(left).append(")>>(").append(right) ;
                break;
            case IN:
                str.append(right).append(").Contains(").append(left);
                //str.append(left).append(") is (").append(right) ;
                break;
            case NOT_IN:
                str.append(right).append(").Contains(").append(left);
                //str.append(left).append(")!is(").append(right) ;
                break;
            case AND:
                str.append(left).append(")&&(").append(right) ;
                break;
            case OR:
                str.append(left).append(")||(").append(right) ;
                break;
            case IFF:
                break;
            default:
                str.append("ERROR");
        }

        if(!(x.op.equals(ExprBinary.Op.JOIN))) {
            str.append(")");
        }

        return new String(str);
    }

    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */ // copied from FuncVisitor
    @Override public String visit(ExprList x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprList visited " + x.toString() + "op=" + x.op.toString());
        //String returnString = "ExprList:";

        StringBuilder str = new StringBuilder("");
        Expr name;
        Iterator<? extends Expr> iterator;
        switch(x.op) {

//            case DISJOINT:
//                break;
//            case TOTALORDER:
//                break;
            case AND:
                iterator = x.args.iterator();
                name = iterator.next();
                str.append(name.accept(this));
                while (iterator.hasNext()) {
                    name = iterator.next();
                    str.append(" && ").append(name.accept(this));
                }
                break;
            case OR:
                iterator = x.args.iterator();
                name = iterator.next();
                str.append(name.accept(this));
                while (iterator.hasNext()) {
                    name = iterator.next();
                    str.append(" || ").append(name.accept(this));
                }
                break;
        }

        return new String(str);

    }

    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprCall x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprCall visited" + x.toString());
        //String returnString = "ExprCall:";
        String returnString = "";
      //  for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
      //  return null;
        //returnString = x.accept(_funcVisitor);


       // for(Expr y:x.args) { returnString = returnString.concat(" arg=" + y.accept(this));}
       // returnString = returnString.concat((x.toString().replaceAll("\\[", "(").replaceAll("\\]", ")").replaceAll("this/","")));

        returnString = returnString.concat("(FuncClass." + x.fun.label.replaceAll("this/", "") + "(" );

        for(Expr y:x.args) {
            returnString = returnString.concat(y.accept(this) + ", ");
        }

        returnString = returnString.substring(0, returnString.length()-2).concat("))"); // delete last comma
        return returnString;


    }

    /** Visits an ExprConstant node (this default implementation simply returns null) */ // copied from FuncVisitor
    @Override public String visit(ExprConstant x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprConstant visited" + x.toString());
        //String returnString = "ExprConst:";
        StringBuilder str = new StringBuilder();

        switch(x.op) {

            case TRUE:
                str.append("True");
                break;
            case FALSE:
                str.append("False");
                break;
//            case IDEN:
//                break;
            case MIN:
                str.append("Int32.MinValue");
                break;
            case MAX:
                str.append("Int32.MaxValue");
                break;
//            case NEXT:
//                break;
//            case EMPTYNESS:
//                break;
            case STRING:
                str.append(x.string);
                break;
            case NUMBER:
                str.append(Integer.toString(x.num));
                break;
        }


        return new String(str);
    }

    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */ // copied from FuncVisitor
    @Override public String visit(ExprITE x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprITE visited" + x.toString());
        StringBuilder str = new StringBuilder("");
        str.append("(").append(x.cond.accept(this)).append(") ? (").append(x.left).append(") : (").append(x.right).append(")");

        return new String(str);
    }

    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
    @Override public String visit(ExprLet x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprLet visited" + x.toString());
        //String returnString = "ExprLet:";
        String returnString = "";
        String ans = x.var.accept(this);
        if (ans==null) ans = x.expr.accept(this);
        if (ans==null) ans = x.sub.accept(this);

        return returnString.concat(ans);
    }

    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
    @Override public String visit(ExprQt x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprQT visited" + x.toString());

        String returnString = "";
         // create output

        String front = "";
        String tail = "";

        for(Decl d: x.decls) {
            for(ExprHasName v: d.names){

                switch(x.op){

                    case ALL:
                        front = front.concat("Contract.ForAll(" + d.expr.accept(_typeVisitor) + "Set, " + v.label + " => ");
                        tail = (")").concat(tail);
                        break;
                    case NO:
                        front = front.concat("(" + d.expr.accept(_typeVisitor) + "Set.Where(" + v.label + " => ");
                        tail = (").Count()==0)").concat(tail);
                        break;
                    case LONE:
                        front = front.concat("(" + d.expr.accept(_typeVisitor) + "Set.Where(" + v.label + " => ");
                        tail = (").Count()<=1)").concat(tail);
                        break;
                    case ONE:
                        front = front.concat("(" + d.expr.accept(_typeVisitor) + "Set.Where(" + v.label + " => ");
                        tail = (").Count()==1) ").concat(tail);
                        break;
                    case SOME:
                        front = front.concat("Contract.Exists(" + d.expr.accept(_typeVisitor) + "Set, " + v.label + " => ");
                        tail = (")").concat(tail);
                        break;
                    case SUM:
                        front = front.concat("SUM("); // TODO
                        tail = (")").concat(tail);
                        break;
                    case COMPREHENSION:
                        front = front.concat("COMPREHENSION("); // TODO
                        tail = (")").concat(tail);
                        break;
                }

            }
        }



    /*    for (Decl d: x.decls){
            returnString = returnString.concat(d.expr.accept(_typeVisitor) + "Set, ");
            for (ExprHasName v: d.names){
                returnString = returnString.concat(v.label +  " "); //returnString.concat((v.accept(this)));

            }
        }
     */
      //  return returnString.concat(x.sub.accept(this));



        returnString = front.concat(x.sub.accept(this)).concat(tail);
        return  returnString;
    }


    /** Visits an ExprUnary node (OP X) by calling accept() on X. */
    @Override public String visit(ExprUnary x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprUnary visited" + x.toString());
        //String returnString = "ExprUnary:";

        String returnString = "";

        String subExpr = x.sub.accept(this);

        switch(x.op){
            case SOMEOF:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count()>0)");
                break;
            case LONEOF:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count()<=1)");
                break;
            case ONEOF:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count()==1");
                break;
            case SETOF:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ")!=null)");
                break;
            case EXACTLYOF:
                returnString = returnString.concat("("  + x.sub.accept(this) + ")");
                break;
            case NOT:
                returnString = returnString.concat("!("  + x.sub.accept(this) + ")");
                break;
            case NO:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count()==0)");
                break;
            case SOME:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count>0)");
                break;
            case LONE:
                returnString = returnString.concat("(("  + x.sub.accept(this) + ").Count<=1)");
                break;
            case ONE:
                returnString = returnString.concat("((" + x.sub.accept(this) + ").Count==1)");
                break;
            case TRANSPOSE:
                returnString = returnString.concat("Helper.Transpose(" + x.sub.accept(this) + ")");
                break;
            case RCLOSURE:
                returnString = returnString.concat("Helper.RClosure(" +  x.sub.accept(this) + ")");
                break;
            case CLOSURE:
                returnString = returnString.concat("Helper.Closure(" + x.sub.accept(this) + ")");
                break;
            case CARDINALITY:
                returnString = returnString.concat(x.sub.accept(this) + ".Count()");
                break;
            case CAST2INT:
                returnString = returnString.concat("(Int32)(" + x.sub.accept(this) + ")");
                break;
            case CAST2SIGINT:
                returnString = returnString.concat("(Integer)("+ x.sub.accept(this) +")");
                break;
            case NOOP:
                returnString = returnString.concat(x.sub.accept(this));
                break;

        }

        return returnString; //+ x.sub.accept((this));
        // return returnString;
    }

    /** Visits a ExprVar node (this default implementation simply returns null) */
    @Override public String visit(ExprVar x) throws Err {
        if(DEBUG) System.out.println("DEBUG: ExprVar visited" + x.toString());
        //String returnString = "ExprVar:";
        String returnString = "";

        return returnString.concat(x.label.replaceAll("this/", ""));
    }

    /** Visits a Sig node (this default implementation simply returns null) */
    @Override public String visit(Sig x) throws Err {
        if(DEBUG) System.out.println("DEBUG: Sig visited" + x.toString());
        //String returnString = "Sig:";
        String returnString = "";
        return returnString.concat(x.label.replaceAll("this/", "").concat("Set"));
    }

    /** Visits a Field node (this default implementation simply returns null) */
    @Override public String visit(Sig.Field x) throws Err {
        if(DEBUG) System.out.println("DEBUG: Sig.Field visited" + x.toString());
        //String returnString = "SigField:";
        String returnString = "";

        return returnString.concat(x.label);
    }
}



