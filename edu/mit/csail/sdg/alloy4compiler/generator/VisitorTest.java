package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.visitsTest.*;


public class VisitorTest extends VisitQuery<String> {

    private int _currDepth;
    final boolean DEBUG = Debug.enableTestOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }

    @Override public String visit(Sig.Field field) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG: "+ field.toString() + " Sig.Field visited with depth " + getDepth());
        String res = FieldVisitTest.visit(field, this);
        decDepth();
        return res;
    }

    @Override public String visit(Sig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig visited with depth " + getDepth());
        String res = SigVisitTest.visit(sig, this);
        decDepth();
        return res;
    }

    public String visit(Sig.SubsetSig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig.SubsetSig visited with depth " + getDepth());
        String res = SigVisitTest.visit(sig, this);
        decDepth();
        return res;
    }
    public String visit(Sig.PrimSig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig.PrimSig visited with depth " + getDepth());
        String res = SigVisitTest.visit(sig, this);
        decDepth();
        return res;
    }

    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */
    @Override public String visit(ExprBinary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprBinary visited with depth " + getDepth());
        String res = ExprBinaryVisitTest.visit(x, this);
        decDepth();
        return res;
    }

    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprList x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprList visited with depth " + getDepth());
        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        return null;
    }

    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprCall x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprCall visited with depth " + getDepth());
        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        return null;
    }

    /** Visits an ExprConstant node (this default implementation simply returns null) */
    @Override public String visit(ExprConstant x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprConstant visited with depth " + getDepth());
        return null;
    }

    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */
    @Override public String visit(ExprITE x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprITE visited with depth " + getDepth());
        String ans = x.cond.accept(this);
        if (ans==null) ans = x.left.accept(this);
        if (ans==null) ans = x.right.accept(this);
        return ans;
    }

    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
    @Override public String visit(ExprLet x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprLet visited with depth " + getDepth());
        String ans = x.var.accept(this);
        if (ans==null) ans = x.expr.accept(this);
        if (ans==null) ans = x.sub.accept(this);
        return ans;
    }

    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
    @Override public String visit(ExprQt x) throws Err {
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprQt visited with depth " + getDepth());

        for(Decl d: x.decls) {
            for(ExprHasName v: d.names) { String ans = v.accept(this); if (ans!=null) return ans; }
            String ans = d.expr.accept(this); if (ans!=null) return ans;
        }

        return x.sub.accept(this);
    }
    @Override public String visit(ExprUnary x) throws Err {

        String retString = "";
        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprUnary visited with depth " + getDepth());

        retString = "unaryexp " + x.sub.accept(this);
        return retString;
    }

    @Override public String visit(ExprVar x) throws Err {
        String retString = "";

        if(DEBUG) System.out.println("DEBUG: "+ x.toString() + " ExprVar visited with depth " + getDepth());
        retString = "varexpr " + x.label + " varexprType = " + x.type() + " varexprString = " + x.toString();
        return retString;
    }
}