package edu.mit.csail.sdg.alloy4compiler.generator;

/**
 * Created by marcel on 4/5/14.
 */
public class Debug {
    public static final boolean enableVisitorOutput = false;
    public static final boolean enableAssertionOutput = false;
    public static final boolean enableExprTypeOutput = false;
    public static final boolean enableFuncOutput = false;
    public static final boolean enableInvariantOutput = false;
    public static final boolean enableMultiplicityOutput = false;
    public static final boolean enableSetOutput = false;
    public static final boolean enableSigOutput = false;
    public static final boolean enableTestOutput = false;
    public static final boolean enableConditionsOutput = false;
    public static final boolean enableFieldOutput = false;
}
