package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;

/**
 * Created by marcel on 4/16/14.
 */
public class VisitorField extends VisitQuery<Boolean> {
    public VisitorField() {
        super();
    }
    final boolean DEBUG = Debug.enableFieldOutput;
    @Override
    public Boolean visit(ExprBinary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(field): "+ x.toString() + " ExprBinary visited");
        if(x.op.equals(ExprBinary.Op.JOIN)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    @Override
    public Boolean visit(ExprUnary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(field): "+ x.toString() + " ExprUnary visited");
        return x.sub.accept(this);
    }
    @Override
    public Boolean visit(Sig.Field x) throws Err {
        if(DEBUG) System.out.println("DEBUG(field): "+ x.toString() + " Field visited");
        return Boolean.TRUE;
    }
    public Boolean visit(Sig x) throws Err {
        if(DEBUG) System.out.println("DEBUG(field): "+ x.toString() + " Sig visited");
        return Boolean.FALSE;
    }

}
