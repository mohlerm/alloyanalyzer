package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.codegens.HelperCodeGen;

import java.util.HashMap;
import java.util.Iterator;


public class VisitorFunc extends VisitQuery<String> {

    private int _currDepth;
    final boolean DEBUG = Debug.enableFuncOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }
    public VisitorFunc() {
        _mapSets = null;
    }
    public VisitorFunc(HashMap<String, Boolean> mapSets) {
        _mapSets = mapSets;
    }
    private HashMap<String, Boolean>  _mapSets;

    @Override public String visit(Sig.Field field) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ field.toString() + " Sig.Field visited with depth " + getDepth());

        decDepth();
        return field.label;
    }
    /** Visits an ExprVar node */
    @Override public String visit(ExprVar x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprVar visited with depth " + getDepth());
        decDepth();
        return x.label;
        //return new ExprObj(null, null, null, null, fieldName, exprVar.type().toString() );
    }
    /** Visits an ExprUnary node */
    @Override public String visit(ExprUnary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprUnary visited with depth " + getDepth());
        StringBuilder str = new StringBuilder("");
        String sub = x.sub.accept(this);

        switch (x.op) {
            case SOMEOF:
                break;
            case LONEOF:
                break;
            case ONEOF:
                break;
            case SETOF:
                break;
//            case EXACTLYOF:
//                break;
            case NOT:
                break;
//            case NO:
//                break;
//            case SOME:
//                break;
//            case LONE:
//                break;
//            case ONE:
//                break;
            case TRANSPOSE:
                HelperCodeGen.transposeNeeded = true;
                str.append("Helper.Transpose(").append(sub).append(")");
                break;
            case RCLOSURE:
                HelperCodeGen.rclosureNeeded = true;
                str.append("Helper.RClosure(").append(sub).append(")");
                break;
            case CLOSURE:
                HelperCodeGen.closureNeeded = true;
                str.append("Helper.Closure(").append(sub).append(")");
                break;
            case CARDINALITY:
                str.append(sub).append(".Count()");
                break;
            case CAST2INT:
                str.append("(Int32)(").append(sub).append(")");
                break;
            case CAST2SIGINT:
                str.append("(Integer)(").append(sub).append(")");
                break;
            case NOOP:
                str.append(sub);
                break;
            default:
        }



        decDepth();
        return new String(str);

    }

//    @Override public String visit(Sig sig) throws Err {
//        incDepth();
//        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig visited with depth " + getDepth());
//        String res = SigVisit.visit(sig, this);
//        decDepth();
//        return res;
//    }

//    public String visit(Sig.SubsetSig sig) throws Err {
//        incDepth();
//        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig.SubsetSig visited with depth " + getDepth());
//        String res = SigVisit.visit(sig, this);
//        decDepth();
//        return res;
//    }
//    public String visit(Sig.PrimSig sig) throws Err {
//        incDepth();
//        if(DEBUG) System.out.println("DEBUG: "+ sig.toString() + " Sig.PrimSig visited with depth " + getDepth());
//        String res = SigVisit.visit(sig, this);
//        decDepth();
//        return res;
//    }

    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */ // copied to AssertionVisitor
    @Override public String visit(ExprBinary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprBinary visited with depth " + getDepth());
        StringBuilder str = new StringBuilder("");
        String left = x.left.accept(this);
        String right = x.right.accept(this);

        VisitorSet visitorSet = new VisitorSet();
        Boolean leftIsSet;
        Boolean rightIsSet;

        if (x.op.equals(ExprBinary.Op.IMPLIES) || x.op.equals(ExprBinary.Op.NOT_IN)){ // implies <=> !(A) | (B)
            str.append("!");
        }

        if(!(x.op.equals(ExprBinary.Op.JOIN))) {
            str.append("(");
        }
        //str.append(left);
        //String operator = "ERROR";
            switch (x.op) {
                case ARROW:
                    break;
                case ANY_ARROW_SOME:
                    break;
                case ANY_ARROW_ONE:
                    break;
                case ANY_ARROW_LONE:
                    break;
                case SOME_ARROW_ANY:
                    break;
                case SOME_ARROW_SOME:
                    break;
                case SOME_ARROW_ONE:
                    break;
                case SOME_ARROW_LONE:
                    break;
                case ONE_ARROW_ANY:
                    break;
                case ONE_ARROW_SOME:
                    break;
                case ONE_ARROW_ONE:
                    break;
                case ONE_ARROW_LONE:
                    break;
                case LONE_ARROW_ANY:
                    break;
                case LONE_ARROW_SOME:
                    break;
                case LONE_ARROW_ONE:
                    break;
                case LONE_ARROW_LONE:
                    break;
                case ISSEQ_ARROW_LONE:
                    break;
                case JOIN:
                    str.append(left).append(".").append(right) ;
                    break;
                case DOMAIN:
                    break;
                case RANGE:
                    break;
                case INTERSECT:

                    if(_mapSets != null) {
                        if(_mapSets.containsKey(x.left.toString())) {
                            leftIsSet = _mapSets.get(x.left.toString());
                        } else {
                            leftIsSet = x.left.accept(visitorSet);
                        }
                        if(_mapSets.containsKey(x.right.toString())) {
                            rightIsSet = _mapSets.get(x.right.toString());
                        } else {
                            rightIsSet = x.right.accept(visitorSet);
                        }
                    } else {
                        leftIsSet = x.left.accept(visitorSet);
                        rightIsSet = x.right.accept(visitorSet);
                    }

                    if(leftIsSet.equals(Boolean.TRUE)) {
                        // TODO: handle properly
                        HelperCodeGen.intersectNeeded = true;
                        str.append("Helper.Intersect(").append(left).append(", ").append(right).append(")");
                    } else {
                        if (rightIsSet.equals(Boolean.TRUE)) {
                            //str.append("Helper.Intersect(").append(sub).append(")");
                            HelperCodeGen.intersectNeeded = true;
                            str.append("Helper.Intersect(").append(left).append(", ").append(right).append(")");
                        } else {
                            str.append(left).append(") && (").append(right);
                        }
                    }

                    break;
                case PLUSPLUS:
                    str.append(left).append(")++(").append(right) ;
                    break;
                case PLUS:

                    if(_mapSets != null) {
                        if(_mapSets.containsKey(x.left.toString())) {
                            leftIsSet = _mapSets.get(x.left.toString());
                        } else {
                            leftIsSet = x.left.accept(visitorSet);
                        }
                        if(_mapSets.containsKey(x.right.toString())) {
                            rightIsSet = _mapSets.get(x.right.toString());
                        } else {
                            rightIsSet = x.right.accept(visitorSet);
                        }
                    } else {
                        leftIsSet = x.left.accept(visitorSet);
                        rightIsSet = x.right.accept(visitorSet);
                    }

                    if(leftIsSet.equals(Boolean.TRUE)) {
                            // TODO: handle properly
                            HelperCodeGen.unionNeeded = true;
                            str.append("Helper.Union(").append(left).append(", ").append(right).append(")");
                    } else {
                        if (rightIsSet.equals(Boolean.TRUE)) {
                            HelperCodeGen.unionNeeded = true;
                            str.append("Helper.Union(").append(left).append(", ").append(right).append(")");
                        } else {
                            str.append(left).append(") || (").append(right);
                        }
                    }
                    break;
                case IPLUS:
                    str.append(left).append(").Integer.Plus(").append(right) ;
                    break;
                case MINUS:

                    if(_mapSets != null) {
                        if(_mapSets.containsKey(x.left.toString())) {
                            leftIsSet = _mapSets.get(x.left.toString());
                        } else {
                            leftIsSet = x.left.accept(visitorSet);
                        }
                        if(_mapSets.containsKey(x.right.toString())) {
                            rightIsSet = _mapSets.get(x.right.toString());
                        } else {
                            rightIsSet = x.right.accept(visitorSet);
                        }
                    } else {
                        leftIsSet = x.left.accept(visitorSet);
                        rightIsSet = x.right.accept(visitorSet);
                    }

                    if(leftIsSet.equals(Boolean.TRUE)) {
                        //TODO: handle properly
                        HelperCodeGen.exceptNeeded = true;
                        str.append("Helper.Except(").append(left).append(", ").append(right).append(")");
                    } else {
                        if (rightIsSet.equals(Boolean.TRUE)) {
                            HelperCodeGen.exceptNeeded = true;
                            str.append("Helper.Except(").append(left).append(", ").append(right).append(")");
                        } else {
                            str.append(left).append(") - (").append(right);
                        }
                    }
                    break;
                case IMINUS:
                    str.append(left).append(").Integer.Plus(").append(right);
                    break;
                case MUL:
                    str.append(left).append(")*(").append(right);
                    break;
                case DIV:
                    str.append(left).append(")/(").append(right);
                    break;
                case REM:
                    str.append(left).append(")%(").append(right);
                    break;
                case EQUALS:
                    str.append(left).append(").Equals(").append(right);
                    break;
                case NOT_EQUALS:
                    str.append(left).append(")!=(").append(right) ;
                    break;
                case IMPLIES:
                    str.append(left).append(") || (").append(right) ;
                    break;
                case LT:
                    str.append(left).append(")<(").append(right) ;
                    break;
                case LTE:
                    str.append(left).append(")<=(").append(right) ;
                    break;
                case GT:
                    str.append(left).append(")>(").append(right) ;
                    break;
                case GTE:
                    str.append(left).append(")>=(").append(right) ;
                    break;
                case NOT_LT:
                    str.append(left).append(")!<(").append(right) ;
                    break;
                case NOT_LTE:
                    str.append(left).append(")!<=(").append(right) ;
                    break;
                case NOT_GT:
                    str.append(left).append(")!>(").append(right) ;
                    break;
                case NOT_GTE:
                    str.append(left).append(")!>=(").append(right) ;
                    break;
                case SHL:
                    str.append(left).append(")<<(").append(right) ;
                    break;
                case SHA:
                    str.append(left).append(")>>(").append(right) ;
                    break;
                case SHR:
                    str.append(left).append(")>>(").append(right) ;
                    break;
                case IN:
                    str.append(right).append(").Contains(").append(left);
                    //str.append(left).append(") is (").append(right) ;
                    break;
                case NOT_IN:
                    str.append(right).append(").Contains(").append(left);
                    //str.append(left).append(")!is(").append(right) ;
                    break;
                case AND:
                    str.append(left).append(")&&(").append(right) ;
                    break;
                case OR:
                    str.append(left).append(")||(").append(right) ;
                    break;
                case IFF:
                    break;
                default:
                    str.append("ERROR");
            }

        if(!(x.op.equals(ExprBinary.Op.JOIN))) {
            str.append(")");
        }

        decDepth();
        return new String(str);
    }

    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */ // copied to AssertionVisitor
    @Override public String visit(ExprList x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprList visited with depth " + getDepth());
        //for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        StringBuilder str = new StringBuilder("");
        Expr name;
        Iterator<? extends Expr> iterator;
        switch(x.op) {

//            case DISJOINT:
//                break;
//            case TOTALORDER:
//                break;
            case AND:
                iterator = x.args.iterator();
                name = iterator.next();
                str.append(name.accept(this));
                while (iterator.hasNext()) {
                    name = iterator.next();
                    str.append(" && ").append(name.accept(this));
                }
                break;
            case OR:
                iterator = x.args.iterator();
                name = iterator.next();
                str.append(name.accept(this));
                while (iterator.hasNext()) {
                    name = iterator.next();
                    str.append(" || ").append(name.accept(this));
                }
                break;
        }

        decDepth();
        return new String(str);
    }

    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprCall x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprCall visited with depth " + getDepth());
        StringBuilder str = new StringBuilder();
        String op = "";
        //str.append(x.fun.toString());

        // hack integer functions
        if((x.fun.toString()).equals("fun integer/plus") || (x.fun.toString()).equals("fun integer/add")) {
            op = " + ";
        } else if ((x.fun.toString()).equals("fun integer/minus") || (x.fun.toString()).equals("fun integer/sub")) {
            op = " - ";
        } else if ((x.fun.toString()).equals("fun integer/mul")) {
            op = " * ";
        } else if ((x.fun.toString()).equals("fun integer/div")) {
            op = " / ";
        } else if ((x.fun.toString()).equals("fun integer/rem")) {
            op = " % ";
        } else if ((x.fun.toString()).equals("fun integer/eq")) {
            op = " == ";
        } else if ((x.fun.toString()).equals("fun integer/gt")) {
            op = " > ";
        } else if ((x.fun.toString()).equals("fun integer/lt")) {
            op = " < ";
        } else if ((x.fun.toString()).equals("fun integer/gte")) {
            op = " >= ";
        } else if ((x.fun.toString()).equals("fun integer/lte")) {
            op = " <= ";
        } else {
            op = x.fun.label;
        }

        str.append("(");
        String temp = "";
        if(x.fun.toString().contains("integer")) {
            for(Expr y:x.args) {
                temp = temp.concat(y.accept(this) + op);
            }
            if(temp.length()>3) {
                temp = temp.substring(0,temp.length()-3);
            }
        } else {
            for(Expr y:x.args) {
                temp = temp.concat(op.split("/")[1] +"(" + y.accept(this)+ ")");
            }
        }

        str.append(temp).append(")");

        decDepth();
        return new String(str);
    }

    /** Visits an ExprConstant node (this default implementation simply returns null) */ // copied to AssertionVisitor
    @Override public String visit(ExprConstant x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprConstant visited with depth " + getDepth());
        StringBuilder str = new StringBuilder();

        switch(x.op) {

            case TRUE:
                str.append("True");
                break;
            case FALSE:
                str.append("False");
                break;
//            case IDEN:
//                break;
            case MIN:
                str.append("Int32.MinValue");
                break;
            case MAX:
                str.append("Int32.MaxValue");
                break;
//            case NEXT:
//                break;
//            case EMPTYNESS:
//                break;
            case STRING:
                str.append(x.string);
                break;
            case NUMBER:
                str.append(Integer.toString(x.num));
                break;
        }

        decDepth();
        return new String(str);
    }

    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */ // copied to FuncVisitor
    @Override public String visit(ExprITE x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprITE visited with depth " + getDepth());
        StringBuilder str = new StringBuilder("");
        str.append("(").append(x.cond.accept(this)).append(") ? (").append(x.left).append(") : (").append(x.right).append(")");
        decDepth();
        return new String(str);
    }

    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
    @Override public String visit(ExprLet x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprLet visited with depth " + getDepth());
        String ans = x.var.accept(this);
        if (ans==null) ans = x.expr.accept(this);
        if (ans==null) ans = x.sub.accept(this);
        decDepth();
        return ans;
    }

    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
    @Override public String visit(ExprQt x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Func): "+ x.toString() + " ExprQt visited with depth " + getDepth());
        for(Decl d: x.decls) {
            for(ExprHasName v: d.names) { String ans = v.accept(this); if (ans!=null) return ans; }
            String ans = d.expr.accept(this); if (ans!=null) return ans;
        }
        decDepth();
        return x.sub.accept(this);
    }
}
