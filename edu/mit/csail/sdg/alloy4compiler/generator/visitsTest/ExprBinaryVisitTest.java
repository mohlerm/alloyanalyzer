package edu.mit.csail.sdg.alloy4compiler.generator.visitsTest;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorTest;

/**
 * Created by marcel on 3/23/14.
 */
public class ExprBinaryVisitTest {
    public static String visit (ExprBinary expr, VisitorTest vis) throws Err {
        int depth = expr.getDepth();

        StringBuilder ans = new StringBuilder();
        ans.append("(");
        ans.append(expr.left.accept(vis)).append("$");
        ans.append(expr.op.toString()).append("$");
        ans.append(expr.right.accept(vis));
        ans.append(")");

        return new String(ans);
    }
}
