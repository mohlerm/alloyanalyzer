package edu.mit.csail.sdg.alloy4compiler.generator.visitsTest;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorTest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcel on 3/16/14.
 */
public class FieldVisitTest  {
    public static String visit (Sig.Field field, VisitorTest vis) throws Err {
        int depth = vis.getDepth();
        String offset = vis.getOffset();

        String fieldName = field.label;

        // definitions string contains field definitions, contracts string contains contracts (e.g. invariants)
        StringBuilder definitions = new StringBuilder();
        StringBuilder contracts = new StringBuilder();

        // check for arity
        String[] typeArr = field.type().toString().split("->");

        if (depth <= 1) {
            // next line is for debug purpose only
            //definitions.append("//field ").append(fieldName).append(" from type ").append(field.type()).append("{\n");


            // definitions setup with proper indentation
            definitions.append(offset);
            // if field private
            if (field.isPrivate != null) {
                definitions.append("private ");
            }

            // TODO implement proper subexpressions

            // if binary relation
            if (typeArr.length == 2) {
                definitions.append("public ").append(typeArr[1].split("/")[1].replace("}", ""));
                contracts.append(offset);
                if (field.oneOf() != null) {
                    contracts.append("\tContract.Invariant(").append(fieldName).append(" != null);\n");
                }
                //else if(field.someOf() != null) {
                //    definitions.append(" SOME");
                //}
                else if (field.loneOf() != null) {
                    definitions.append(" LONE");
                }
            }
            // if ternary relation
            if (typeArr.length == 3) {
                // figure out the sub expression names with the visitor
                String subex;
                subex = (field.decl().expr.accept(vis));

                // DEBUG code
                // TODO remove
                //System.out.println(subex);

                //definitions.append(offset).append(field.decl().expr.accept(vis));
                definitions.append("public ISet<Tuple<").append(typeArr[1].split("/")[1].replace("}", "")).append(", ");
                definitions.append(typeArr[2].split("/")[1].replace("}", "")).append(">>");
                // also add new invariant
                //System.out.println("DEBUG NAME: "+ field.toString());
                // TODO add proper invariant
                contracts.append("\t\tTODO Contract.Invariant(").append(fieldName).append(" != null);\n");

            }

            definitions.append(" ").append(fieldName).append(";\n");
        }
        else {
            definitions.append(typeArr[1].split("/")[1].replace("}", ""));
            contracts.append(fieldName);
        }



        return  (new String(definitions) + "#" + new String(contracts));
    }

    private static String parseSubex (String subex) throws Err {
        StringBuilder ret = new StringBuilder();
        String[] split = subex.split("$");


        return new String(ret);
    }
}
