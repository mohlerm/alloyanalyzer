package edu.mit.csail.sdg.alloy4compiler.generator.visitsTest;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorTest;

/**
 * Created by marcel on 3/16/14.
 */
public class SigVisitTest {
    public static String visit (Sig sig, VisitorTest vis) throws Err {
        StringBuilder definitions = new StringBuilder();
        String offset = vis.getOffset();

        // if it's an "inner sig"
        if(vis.getDepth()>0) {
            String sigName = sig.label.split("/")[1];
            if (sig.isAbstract != null) {
                definitions.append("abstract ");
            }

            definitions.append("inner SIG ").append(sigName);
            if(sig.isSome != null) {
                definitions.append(" SOME");
            }
            else if(sig.isOne != null) {
                definitions.append(" ONE");
            }
            else if(sig.isLone != null) {
                definitions.append(" LONE");
            }

            definitions.append("\n");

            //System.out.println("DEBUG: " + sig.toString() + " Sig visited with depth " + sig.getDepth());
        }


        // if sig is an outer sig
        else {


            String currSigName = sig.label.split("/")[1];
            if (sig.isAbstract != null) {
                definitions.append("abstract ");
            }
            definitions.append("public class ").append(currSigName);

            if(sig.isSubsig != null) {
                Sig parent = ((Sig.PrimSig)sig).parent;
                if(parent!=Sig.UNIV) {
                    definitions.append(" : ").append(parent.label.split("/")[1]);
                }
            }

            definitions.append(" {\n");

            StringBuilder fieldDefinitions = new StringBuilder();
            StringBuilder fieldContracts = new StringBuilder();
            // get all fields
            for (Expr s : sig.getFields()) {
                String temp = s.accept(vis);
                if (temp.contains("#")) {
                    String[] tempArr = temp.split("#");
                    fieldDefinitions.append(tempArr[0]);
                    fieldContracts.append(tempArr[1]);
                }
            }

            if (fieldDefinitions.length() > 0 && fieldContracts.length() > 0) {
                definitions.append(fieldDefinitions);
                definitions.append("\n\t[ContractInvariantMethod]\n\tprivate void ObjectInvariant() {\n");
                definitions.append(fieldContracts);
            }

            // TODO facts
            //        StringBuilder[] factsArr = new StringBuilder[2];
            //        // get all fields
            //        for(Expr s:sig.getFields()) {
            //            String[] temp = s.accept(vis);
            //            factsArr[0].append(temp[0]);
            //            factsArr[1].append(temp[1]);
            //        }
            //        definitions.append(factsArr[0]);
            //        definitions.append("[ContractInvariantMethod]\nprivate void ObjectInvariant() {)\n");
            //        definitions.append(factsArr[1]);

//            //no need to check for lone since that's not part of that project
//            if (sig.isLone != null) {
//                definitions.append("//LONE\n");
//            }
            //check for one
            else if (sig.isOne != null) {
                definitions.append("\tprivate static ").append(currSigName).append(" instance\n");
                definitions.append("\tprivate ").append(currSigName).append("() { }\n");
                definitions.append("\tpublic static ").append(currSigName).append(" Instance {\n");
                definitions.append("\t\tget {\n");
                definitions.append("\t\t\tif (instance == null) {\n\t\t\t\tinstance = new ").append(currSigName);
                definitions.append("();\n\t\t\t}\n\t\t\treturn instance\n\t\t}\n\t}\n");
            }
//            //no need to check for some since that's not part of that project
//            else if (sig.isSome != null) {
//                definitions.append("//SOME\n");
//            }

            definitions.append("}\n\n");
        }

        //sig.expr.accept(vis);

        //_out.print(new String(definitions));
        return new String(definitions);
    }
}
