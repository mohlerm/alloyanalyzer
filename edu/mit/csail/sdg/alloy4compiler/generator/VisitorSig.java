package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

/**
 * Created by marcel on 4/2/14.
 */
public class VisitorSig extends VisitQuery<Sig> {
    public VisitorSig() {
        super();
    }
    final boolean DEBUG = Debug.enableSigOutput;
    @Override
    public Sig visit(ExprBinary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Sig): "+ x.toString() + " ExprBinary visited");
        switch(x.op) {

            case ARROW:
                break;
            case ANY_ARROW_SOME:
                break;
            case ANY_ARROW_ONE:
                break;
            case ANY_ARROW_LONE:
                break;
            case SOME_ARROW_ANY:
                break;
            case SOME_ARROW_SOME:
                break;
            case SOME_ARROW_ONE:
                break;
            case SOME_ARROW_LONE:
                break;
            case ONE_ARROW_ANY:
                break;
            case ONE_ARROW_SOME:
                break;
            case ONE_ARROW_ONE:
                break;
            case ONE_ARROW_LONE:
                break;
            case LONE_ARROW_ANY:
                break;
            case LONE_ARROW_SOME:
                break;
            case LONE_ARROW_ONE:
                break;
            case LONE_ARROW_LONE:
                break;
            case ISSEQ_ARROW_LONE:
                break;
            case JOIN:
                break;
            case DOMAIN:
                break;
            case RANGE:
                break;
            case INTERSECT:
                return ((Sig.PrimSig)x.right.accept(this)).intersect((Sig.PrimSig)x.left.accept(this));
            case PLUSPLUS:
                break;
            case PLUS:
                return ((Sig.PrimSig)x.right.accept(this)).leastParent((Sig.PrimSig)x.left.accept(this));
            case IPLUS:
                break;
            case MINUS:
                return ((Sig.PrimSig)x.right.accept(this)).leastParent((Sig.PrimSig)x.left.accept(this));
            case IMINUS:
                break;
            case MUL:
                break;
            case DIV:
                break;
            case REM:
                break;
            case EQUALS:
                break;
            case NOT_EQUALS:
                break;
            case IMPLIES:
                break;
            case LT:
                break;
            case LTE:
                break;
            case GT:
                break;
            case GTE:
                break;
            case NOT_LT:
                break;
            case NOT_LTE:
                break;
            case NOT_GT:
                break;
            case NOT_GTE:
                break;
            case SHL:
                break;
            case SHA:
                break;
            case SHR:
                break;
            case IN:
                break;
            case NOT_IN:
                break;
            case AND:
                break;
            case OR:
                break;
            case IFF:
                break;
        }
        return ((Sig.PrimSig)x.right.accept(this)).leastParent((Sig.PrimSig)x.left.accept(this));
    }
    public Sig visit(ExprUnary x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Sig): "+ x.toString() + " ExprUnary visited");
        //if(x.label)
        //return (Sig)x.sub;
        // TODO: incredibly ugly
        if(x.sub.toString().equals("this")) {
            return Sig.UNIV;
        }
        return x.sub.accept(this);
    }
    public Sig visit(Sig.Field x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Sig): "+ x.toString() + " Field visited");
        return x.decl().expr.accept(this);
    }
    public Sig visit(Sig x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Sig): "+ x.toString() + " Sig visited");
        return x;
    }

}
