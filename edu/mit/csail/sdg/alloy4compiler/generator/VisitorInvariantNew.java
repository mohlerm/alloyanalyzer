package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

/**
 * Created by Fabian on 15.04.14.
 */
public class VisitorInvariantNew extends VisitQuery<String>  {

    public VisitorInvariantNew() {
        _currDepth = 0;
    }

    private int _currDepth;
    final boolean DEBUG = Debug.enableInvariantOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }

    public String rootLabel;
    public String currentLabel;

    @Override public String visit(Sig.Field field) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ field.toString() + " Sig.Field visited with depth " + getDepth());
        StringBuilder res = new StringBuilder("");
        currentLabel = field.label;
        if(getDepth() <= 1) {
            rootLabel = field.label;

            res.append(field.decl().expr.accept(this));
//            if(!inv.isEmpty()) {
//                res.append("\t\tContract.Invariant(").append(inv).append(");\n");
//            }

        } else {
            res.append(field.decl().expr.accept(this));
        }
        decDepth();
        return new String(res);
    }
    /** Visits an ExprVar node */
    @Override public String visit(ExprVar x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprVar visited with depth " + getDepth());

        StringBuilder res = new StringBuilder(x.toString());

        decDepth();
        return new String(res);
    }
    /** Visits an ExprUnary node */
    @Override public String visit(ExprUnary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprUnary visited with depth " + getDepth());

        StringBuilder res = new StringBuilder();
        switch (x.op) {
            // TODO: remove commented lines
            case SOMEOF:
                res.append("\t\tContract.Invariant(").append(this.currentLabel).append(" != null && ").append(this.currentLabel).append(".Count() > 0);\n");
                break;
            case LONEOF:
                // nothing needed
                break;
            case ONEOF:
                // not null
                res.append(this.currentLabel).append(" != null");
                break;
            case SETOF:
                res.append(this.currentLabel).append(" != null");
                break;
            case EXACTLYOF:
                break;
            case NOT:
                break;
            case NO:
                break;
            case SOME:
                break;
            case LONE:
                break;
            case ONE:
                break;
            case TRANSPOSE:
                break;
            case RCLOSURE:
                break;
            case CLOSURE:
                break;
            case CARDINALITY:
                break;
            case CAST2INT:
                break;
            case CAST2SIGINT:
                break;
            case NOOP:
                res.append(x.sub.accept(this));
                break;
        }
        decDepth();
        return new String(res);
    }

    @Override public String visit(Sig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ sig.toString() + " Sig visited with depth " + getDepth());
        //String res = SigVisit.visit(sig, this);
        //currentLabel = sig.label.split("/")[1];
        decDepth();
        //return sig.type().toString().split("/")[1].replace("}","");
        //return sig.label.split("/")[1] + " != null ";
        return "";
    }

    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */
    @Override public String visit(ExprBinary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprBinary visited with depth " + getDepth());

        StringBuilder res = new StringBuilder("");
        VisitorMultiplicity visMult = new VisitorMultiplicity();

        String leftMult = (x.left.accept(visMult));

        String rightMult = (x.right.accept(visMult));

        switch (x.op) {

            case ARROW:
                break;
            case ANY_ARROW_SOME:
                break;
            case ANY_ARROW_ONE:
                break;
            case ANY_ARROW_LONE:
                break;
            case SOME_ARROW_ANY:
                break;
            case SOME_ARROW_SOME:
                break;
            case SOME_ARROW_ONE:
                break;
            case SOME_ARROW_LONE:
                break;
            case ONE_ARROW_ANY:
                break;
            case ONE_ARROW_SOME:
                break;
            case ONE_ARROW_ONE:
                break;
            case ONE_ARROW_LONE:
                break;
            case LONE_ARROW_ANY:
                break;
            case LONE_ARROW_SOME:
                break;
            case LONE_ARROW_ONE:
                break;
            case LONE_ARROW_LONE:
                break;
            case ISSEQ_ARROW_LONE:
                break;
            case JOIN:
                break;
            case DOMAIN:
                break;
            case RANGE:
                break;
            case INTERSECT:
                break;
            case PLUSPLUS:
                break;
            case PLUS:
                break;
            case IPLUS:
                break;
            case MINUS:
                break;
            case IMINUS:
                break;
            case MUL:
                break;
            case DIV:
                break;
            case REM:
                break;
            case EQUALS:
                break;
            case NOT_EQUALS:
                break;
            case IMPLIES:
                break;
            case LT:
                break;
            case LTE:
                break;
            case GT:
                break;
            case GTE:
                break;
            case NOT_LT:
                break;
            case NOT_LTE:
                break;
            case NOT_GT:
                break;
            case NOT_GTE:
                break;
            case SHL:
                break;
            case SHA:
                break;
            case SHR:
                break;
            case IN:
                break;
            case NOT_IN:
                break;
            case AND:
                break;
            case OR:
                break;
            case IFF:
                break;
        }
        decDepth();

        return new String(res);
    }

    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprList x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprList visited with depth " + getDepth());
        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        return null;
    }

    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
    @Override public String visit(ExprCall x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprCall visited with depth " + getDepth());
        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
        return null;
    }

    /** Visits an ExprConstant node (this default implementation simply returns null) */
    @Override public String visit(ExprConstant x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprConstant visited with depth " + getDepth());
        return null;
    }

    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */
    @Override public String visit(ExprITE x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprITE visited with depth " + getDepth());
        String ans = x.cond.accept(this);
        if (ans==null) ans = x.left.accept(this);
        if (ans==null) ans = x.right.accept(this);
        return ans;
    }

    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
    @Override public String visit(ExprLet x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprLet visited with depth " + getDepth());
        String ans = x.var.accept(this);
        if (ans==null) ans = x.expr.accept(this);
        if (ans==null) ans = x.sub.accept(this);
        return ans;
    }

    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
    @Override public String visit(ExprQt x) throws Err {
        if(DEBUG) System.out.println("DEBUG(Invariant): "+ x.toString() + " ExprQt visited with depth " + getDepth());
        for(Decl d: x.decls) {
            for(ExprHasName v: d.names) { String ans = v.accept(this); if (ans!=null) return ans; }
            String ans = d.expr.accept(this); if (ans!=null) return ans;
        }
        return x.sub.accept(this);
    }
}
