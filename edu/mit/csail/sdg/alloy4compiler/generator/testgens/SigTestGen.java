package edu.mit.csail.sdg.alloy4compiler.generator.testgens;

import edu.mit.csail.sdg.alloy4.ConstList;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.Pair;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorExprType;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorTest;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorAssertion;
import edu.mit.csail.sdg.alloy4compiler.generator.VisitorSet;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by marcel on 3/12/14.
 */
public class SigTestGen implements ITestGen {

    private PrintWriter _out;
    private ConstList<Sig> _sigList;
    private ConstList<Pair<String, Expr>> _assertions;
    private ConstList<Command> _cmds;
    private ConstList<Command> _checks;
    private A4Solution _solution;
    private A4Solution _nextSolution;
    private LinkedList<String> _singleton = new LinkedList<String>();
    private VisitorTest _visitor;
    private VisitorAssertion _assertionVisitor;
    private VisitorExprType _typeVisitor;
    private VisitorSet _setVisitor;

    public SigTestGen (A4Solution solution, Iterable<Sig> sigs, Iterable<Pair<String, Expr>> assertions, Iterable<Command> cmds, PrintWriter out){

        _out = out;
        _sigList = ConstList.make(sigs);
        _assertions = ConstList.make(assertions);
        _cmds = ConstList.make(cmds);
        _solution = solution;
        _visitor = new VisitorTest();
        _assertionVisitor = new VisitorAssertion();
        _nextSolution = _solution;
        _typeVisitor = new VisitorExprType();
        _setVisitor = new VisitorSet();

        // list with all check commands
        _checks = ConstList.make(cmds);

        for(Command c:_checks){
            if (!c.check)
                _checks.remove(c);
        }
    }

    public void generate() {
        //TODO implement sig test gen

    /*  // get another solution to test
        for (int i = 0; i < 12; i++){
            try {
                _nextSolution = _nextSolution.next();
               // _out.printf(_nextSolution.toString());
            } catch (Err err) {
                err.printStackTrace();
            }
        }
        _out.printf(_nextSolution.toString());
        //_out.printf(_solution.toString());
        _solution = _nextSolution;
    */


        // [data creation section]
        _out.println("\t\t// setup test data\n");

        //create a list of all _singleton names, used for distinction between new() / Instance() creation
       for (Sig s:_solution.getAllReachableSigs()){
            if(s.isOne != null)
            _singleton.addLast(s.label.split("/")[1]);
        }

        String [] solution_lines = _solution.toString().split("\n"); // split solution into lines
        String sig_line; // a line which starts with "this/", (second part, without "this/")


        /// create final Strings for objects, relations and assertions
        String objectString = "";
        String relationString = "";
        String assertionString = "";

        // distinguish between object or relation
        for (String s :solution_lines){
            if (s.split("/")[0].equals("this")){ //solution_line describes Signature to instantiate
                sig_line = s.split("/")[1];

                if (sig_line.contains("<:"))
                    relationString = relationString.concat(createRelation(sig_line));
                else
                    objectString = objectString.concat(createObject(sig_line));

            }
        }

        _out.print(objectString);
        _out.print(relationString);


        // [assertion creation section]
        _out.println("\n\t\t// check test data\n");

        assertionString = createAssertion();
        _out.print(assertionString);



    }

    private String createObject(String obj_line){

        // _out.println("in createObjetc");

        String classType;
        String objectNameArr[];
        String resultString = "";

        String obj_line_arr [] = obj_line.split("=\\{");
        classType = obj_line_arr[0];

        // create Set
        resultString = resultString.concat("\t\t" + "var " + classType + "Set = new HashSet<" + classType + ">();\n");

        obj_line_arr[1] = obj_line_arr[1].replaceAll("\\}", ""); //delete } at end

       // _out.println(classType + ": " + obj_line_arr[1]);
        if (! obj_line_arr[1].isEmpty()){ // set not empty

            String obj_inst;
            for (String s:(obj_line_arr[1].split(", "))){ // split set into object names

                objectNameArr = s.split("\\$");

                if(objectNameArr[0].equals(classType)){ //not ancestor class, object gets instantiated with new() method

                    if (! _singleton.contains(classType)) {// not a _singleton
                        resultString = resultString.concat("\t\t" + classType + " " + objectNameArr[0]+objectNameArr[1] + ";\n");
                        resultString = resultString.concat("\t\t" + classType + "Set.Add(" + objectNameArr[0]+objectNameArr[1]  +  " = new " + classType + "());\n");
                    }else{
                        resultString = resultString.concat("\t\t" + classType + " " + objectNameArr[0]+objectNameArr[1] + ";\n");
                        resultString = resultString.concat("\t\t" + classType + "Set.Add(" + objectNameArr[0]+objectNameArr[1]  + " = " + classType + ".Instance);\n");
                    }

                } else { // ancestor class, object gets instantiated with Instance method
                    resultString = resultString.concat("\t\t" + classType + "Set.Add(" + objectNameArr[0]+objectNameArr[1] + ");\n");

                }

            }
        } else // set empty
        {  // _out.println("the set " + classType + " is empty.");
        }

        return resultString;

    }

    private String createRelation(String line){

        String resultString = "";
        String relation_line_arr [] = line.split("<:|=\\{");
        String relationName = relation_line_arr[1];
        String className = relation_line_arr[0];
        String relationDefiniton = "";
        Boolean fieldIsSet = false;
        String fieldType = "";
        //_out.println(relationName);
        //_out.println(className);

        // get relation Definition
        for (Sig s: _sigList){
            //_out.println(s.label);
            if (s.label.equals("this/" + className)){
                for(Sig.Field f: s.getFields()){
                    //_out.println(f.label);
                    if (f.label.equals(relationName)){

                        try {
                            relationDefiniton = f.accept(_visitor);
                        } catch (Err err) {
                            err.printStackTrace();
                        }
                        try {
                            fieldIsSet = f.accept(_setVisitor);
                        } catch (Err err) {
                            err.printStackTrace();
                        }
                        try {
                            fieldType = f.accept(_typeVisitor);
                            fieldType = fieldType.replaceAll("ISet<", "").replaceAll(">","");
                        } catch (Err err) {
                            err.printStackTrace();
                        }

                        if (relationDefiniton.contains("Tuple")){ // ternary relation
                            resultString = resultString.concat(ternaryRelation(line, relationDefiniton));

                        }else{ //binary relation

                            resultString = resultString.concat(binaryRelation(line, relationName, fieldIsSet, fieldType));

                        }

                    }

                }
            }
        }




        return resultString;
    }

    private String binaryRelation(String line, String relationName, Boolean fieldIsSet, String fieldType){ // line without "this/"

        String resultString = "";
        String relation_line_arr [] = line.split("<:|=\\{");
        relation_line_arr[2] = relation_line_arr[2].replaceAll("\\}", ""); // delete } at end

        String setInitialisation = "";
        String addObjectsToSet = "";

        if(! relation_line_arr[2].isEmpty()){ //set of relation is empty



            for (String s: relation_line_arr[2].split(", ")){

                String Obj1 = s.split("->")[0].replaceAll("\\$", ""); // get clean name of obj1
                String Obj2 = s.split("->")[1].replaceAll("\\$", ""); // get clean name of obj2

                if (fieldIsSet){    // field is set, a.b.Add(...);

                    setInitialisation  = setInitialisation.concat("\t\t" + Obj1 + "." + relationName + " = new HashSet<" + fieldType + ">();\n" );
                    addObjectsToSet = addObjectsToSet.concat("\t\t" + Obj1 + "." + relationName + ".Add(" + Obj2 + ");\n");

                }else{ // field is not a set, a.b = ...;
                    resultString = resultString.concat("\t\t" + Obj1 + "." + relationName + " = " + Obj2 + ";\n");
                }
            }
        }

        return resultString.concat(setInitialisation + addObjectsToSet);
    }

    private String ternaryRelation(String line, String relationDefinition){ // line without "this/"
        String resultString = "";
        String relation_line_arr [] = line.split("<:|=\\{"); // Man <: between ={ Man$0->Platform$0->Platform$0, Man$1->Platform$0->Platform$0}
        relation_line_arr[2] = relation_line_arr[2].replaceAll("\\}", ""); // delete } at end
        String relationName = relation_line_arr[1];

        String setInitialisation = "";
        String addObjectsToSet = "";


        // create relation Set
        String tupleDef = relationDefinition.split("ISet<|> ")[1];
       // _out.println(tupleDef);

        if(! relation_line_arr[2].isEmpty()){ //set of relation is empty

            for (String s: relation_line_arr[2].split(", ")){

                String Obj1 = s.split("->")[0].replaceAll("\\$", ""); // get clean name of obj1
                String Obj2 = s.split("->")[1].replaceAll("\\$", ""); // get clean name of obj2
                String Obj3 = s.split("->")[2].replaceAll("\\$", ""); // get clean name of obj3

                setInitialisation = setInitialisation.concat("\t\t" + Obj1 + "." + relationName + " = new HashSet<" + tupleDef + ">();\n");
                addObjectsToSet = addObjectsToSet.concat("\t\t" + Obj1 + "." + relationName + ".Add(Tuple.Create"  + "(" + Obj2 + ", " + Obj3 + "));\n");
            }
        }


        return resultString.concat(setInitialisation + addObjectsToSet);
    }

    private String createAssertion(){

        String resultString = "";
        // only handle assertions with a check command
        for (Pair<String, Expr> p:_assertions){

            Boolean assertion_has_check = false;

            for (Command c:_checks){
                if(p.a.equals(c.label)){
                    assertion_has_check = true;
                }
            }

            if (assertion_has_check){

               // _out.println(p.b.type());
               // _out.println (p.b.toString());

                try {

                    resultString = resultString.concat("\t\tContract.Assert(" + p.b.accept(_assertionVisitor));
                } catch (Err err) {
                    err.printStackTrace();
                }
                resultString = resultString.concat(", \"" + p.a + "\");\n");
            }





        }

        //_out.println(resultString);
        return (resultString);
    }

}
