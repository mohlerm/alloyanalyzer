package edu.mit.csail.sdg.alloy4compiler.generator.testgens;

/**
 * Created by marcel on 3/14/14.
 */
public interface ITestGen {
    abstract public void generate();
}
