package edu.mit.csail.sdg.alloy4compiler.generator.testgens;

import java.io.PrintWriter;

/**
 * Created by marcel on 3/12/14.
 */
public class MainTestGen implements ITestGen {
    private String _originalFilename;
    private PrintWriter _out;

    public MainTestGen(String originalFilename, PrintWriter out) {
        _originalFilename = originalFilename;
        _out = out;
    }
    public void generate() {
        String[] temp = _originalFilename.split("/");
        String className = temp[temp.length-1];
        className = className.split("\\.")[0];
        _out.println("// "+ className);

        _out.println("using System;");
        _out.println("using System.Linq;");
        _out.println("using System.Collections.Generic;");
        _out.println("using System.Diagnostics.Contracts;\n");



        _out.println("public static class Test");
        _out.println("{");
        _out.println("\tpublic static void Main(string[] args)");
        _out.println("\t{");
    }

    public void generateEnd() {
        _out.println("\t}");
        _out.println("}");
    }
}
