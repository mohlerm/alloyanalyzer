package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

/**
 * Created by marcel on 4/11/14.
 */
public class VisitorConditions extends VisitQuery<String> {
    @Deprecated
    public VisitorConditions() {
        resultType = null;
        _currDepth = 0;
    }
    public VisitorConditions(String res, boolean isRes, boolean in_isSet) {

        if(isRes) {
        condition = "Ensures";
        contractSet = "Contract.Result<" + res + ">()";
        } else {
            condition = "Requires";
            contractSet = res;
        }
        isSet = in_isSet;
        _currDepth = 0;
    }

    private int _currDepth;
    final boolean DEBUG = Debug.enableConditionsOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }

    public String resultType;
    public String currentLabel;
    public String condition;
    public String contractSet;
    public boolean isSet;

    /** Visits an ExprVar node */
    @Override public String visit(ExprVar x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Result): "+ x.toString() + " ExprVar visited with depth " + getDepth());
        decDepth();
        return x.toString();
    }
    /** Visits an ExprUnary node */
    @Override public String visit(ExprUnary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Result): "+ x.toString() + " ExprUnary visited with depth " + getDepth());
        StringBuilder res = new StringBuilder();
//        if(res != null) {
//            contractSet = "" + contractSet + "";
//        } else {
//            contractSet = "TEST";
//        }

        switch (x.op) {
            // TODO: remove commented lines
            case SOMEOF:
                res.append("\t\tContract." + condition + "(" + contractSet + " != null);\n");
                if(isSet) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() > 0);\n");

                }
                res.append(x.sub.accept(this));
                break;
            case LONEOF:
                if(isSet) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() <= 1);\n");
                }
                res.append(x.sub.accept(this));
                break;
            case ONEOF:
                // not null
                res.append("\t\tContract." + condition + "(" + contractSet + " != null);\n");
                if(isSet) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() == 1);\n");
                }
                res.append(x.sub.accept(this));
                break;
            case SETOF:
                res.append("\t\tContract." + condition + "(" + contractSet + " != null);\n");
                res.append(x.sub.accept(this));
                break;
            case EXACTLYOF:
                break;
            case NOT:
                break;
            case NO:
                break;
            case SOME:
                break;
            case LONE:
                break;
            case ONE:
                break;
            case TRANSPOSE:
                break;
            case RCLOSURE:
                break;
            case CLOSURE:
                break;
            case CARDINALITY:
                break;
            case CAST2INT:
                break;
            case CAST2SIGINT:
                break;
            case NOOP:
                res.append(x.sub.accept(this));
                break;
        }
        decDepth();

        return new String(res);
    }

    @Override public String visit(Sig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Result): "+ sig.toString() + " Sig visited with depth " + getDepth());
        //String res = SigVisit.visit(sig, this);
        decDepth();
        //return sig.type().toString().split("/")[1].replace("}","");
        return "";
    }

    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */
    @Override public String visit(ExprBinary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Result): "+ x.toString() + " ExprBinary visited with depth " + getDepth());
        StringBuilder res = new StringBuilder("");
        VisitorMultiplicity visMult = new VisitorMultiplicity();
        VisitorSig visitorSig = new VisitorSig();

        //String leftMult = (x.left.accept(visMult));
        // it's a set per default
        String leftMult = "set";
        //String rightMult = (x.right.accept(visMult));
        // it's a set per default
        String rightMult = "set";

        switch (x.op) {

            case ARROW:
                break;
            case ANY_ARROW_SOME:
                rightMult = "some";
                break;
            case ANY_ARROW_ONE:
                rightMult = "one";
                break;
            case ANY_ARROW_LONE:
                rightMult = "lone";
                break;
            case SOME_ARROW_ANY:
                leftMult = "some";
                break;
            case SOME_ARROW_SOME:
                leftMult = "some";
                rightMult = "some";
                break;
            case SOME_ARROW_ONE:
                leftMult = "some";
                rightMult = "one";
                break;
            case SOME_ARROW_LONE:
                leftMult = "some";
                rightMult = "lone";
                break;
            case ONE_ARROW_ANY:
                leftMult = "one";
                break;
            case ONE_ARROW_SOME:
                leftMult = "one";
                rightMult = "some";
                break;
            case ONE_ARROW_ONE:
                leftMult = "one";
                rightMult = "one";
                break;
            case ONE_ARROW_LONE:
                leftMult = "one";
                rightMult = "lone";
                break;
            case LONE_ARROW_ANY:
                leftMult = "lone";
                break;
            case LONE_ARROW_SOME:
                leftMult = "lone";
                rightMult = "some";
                break;
            case LONE_ARROW_ONE:
                leftMult = "lone";
                rightMult = "one";
                break;
            case LONE_ARROW_LONE:
                leftMult = "lone";
                rightMult = "lone";
                break;
//            case ISSEQ_ARROW_LONE:
//                break;
            case JOIN:
                //TODO: wtf
                //res.append(this.rootLabel).append(" != null && ").append(x.right.accept(this));
                break;
            case DOMAIN:
                break;
            case RANGE:
                break;
            case INTERSECT:
                res.append("\t\tContract." + condition + "(" + contractSet + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " && " + contractSet + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");
                break;
            case PLUSPLUS:
                break;
            case PLUS:
                res.append("\t\tContract." + condition + "(" + contractSet + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " || " + contractSet + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");
                break;
            case IPLUS:
                break;
            case MINUS:
                res.append("\t\tContract." + condition + "(" + contractSet + " is " + x.left.accept(visitorSig).toString().split("/")[1] + " || " + contractSet + " is " + x.right.accept(visitorSig).toString().split("/")[1] + ");\n");
                break;
            case IMINUS:
                break;
            case MUL:
                break;
            case DIV:
                break;
            case REM:
                break;
            case EQUALS:
                break;
            case NOT_EQUALS:
                break;
            case IMPLIES:
                break;
            case LT:
                break;
            case LTE:
                break;
            case GT:
                break;
            case GTE:
                break;
            case NOT_LT:
                break;
            case NOT_LTE:
                break;
            case NOT_GT:
                break;
            case NOT_GTE:
                break;
            case SHL:
                break;
            case SHA:
                break;
            case SHR:
                break;
            case IN:
                break;
            case NOT_IN:
                break;
            case AND:
                break;
            case OR:
                break;
            case IFF:
                break;
        }

        if(this.currentLabel == null)
        {
            this.currentLabel = "NAMEERROR";
        }

        if(isSet) {
            //res.append(this.rootLabel + " != null");
            res.append("\t\tContract." + condition + "(" + contractSet + " != null);\n");
            if(leftMult != null && rightMult != null) {
                if( DEBUG) System.out.println("MULT: LEFT: " + leftMult);
                if( DEBUG) System.out.println("MULT: RIGHT: " + rightMult);
                if (leftMult.equals("one")) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() == 0 || " + "Contract.Exists(" + contractSet + ", r => Contract.ForAll(" + contractSet + ", s => s.Item1.Equals(r.Item1))));\n");
                } else if (leftMult.equals("lone")) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() == 0 || " + "Contract.Exists(" + contractSet + ", r => Contract.ForAll(" + contractSet + ", s => s.Item1.Equals(r.Item1))));\n");
                } else if (leftMult.equals("some")) {
                    res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item1 != null));\n");
                } else if (leftMult.equals("set")) {
                    //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item1 != null));\n");
                }

                if (rightMult.equals("one")) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() == 0 || " + "Contract.Exists(" + contractSet + ", r => Contract.ForAll(" + contractSet + ", s => s.Item2.Equals(r.Item2))));\n");
                } else if (rightMult.equals("lone")) {
                    res.append("\t\tContract." + condition + "(" + contractSet + ".Count() == 0 || " + "Contract.Exists(" + contractSet + ", r => Contract.ForAll(" + contractSet + ", s => s.Item2.Equals(r.Item2))));\n");
                } else if (rightMult.equals("some")) {
                    res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item2 != null));\n");
                } else if (rightMult.equals("set")) {
                    //res.append("\t\tContract." + condition + "(Contract.ForAll(" + contractSet + ", m => m.Item2 != null));\n");
                }

            }
        }


        decDepth();
        return new String(res);

    }
}
