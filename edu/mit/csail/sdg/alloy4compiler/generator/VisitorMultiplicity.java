package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.*;

/**
 * Created by marcel on 4/01/14.
 */
public class VisitorMultiplicity extends VisitQuery<String> {

    private int _currDepth;
    final boolean DEBUG = Debug.enableMultiplicityOutput;

    // public
    public int getDepth() {
        return _currDepth;
    }
    private void setDepth(int depth) {
        _currDepth = depth;
    }
    private void incDepth() {
        _currDepth++;
    }
    private void decDepth() {
        _currDepth--;
    }
    public void zeroingDepth() {
        _currDepth = -1;
    }
    public String getOffset() {
        StringBuilder res = new StringBuilder();
        for (int j = 0; j < _currDepth; j++) {
            res.append("\t");
        }
        return new String(res);
    }

    public String topFieldLabel;

    @Override public String visit(Sig.Field field) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ field.toString() + " Sig.Field visited with depth " + getDepth());
        String mult = field.decl().expr.accept(this);
        decDepth();
        return mult;
    }
//    /** Visits an ExprVar node */
//    @Override public String visit(ExprVar x) throws Err {
//        incDepth();
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprVar visited with depth " + getDepth());
//        String res = "error";
//        decDepth();
//        return res;
//    }
    /** Visits an ExprUnary node */
    @Override public String visit(ExprUnary x) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprUnary visited with depth " + getDepth());
        String res = null;

        switch(x.op) {

            case SOMEOF:
                res = "some";
                break;
            case LONEOF:
                res = "lone";
                break;
            case ONEOF:
                res = "one";
                break;
            case SETOF:
                res = "set";
                break;
            case EXACTLYOF:
                break;
            case NOT:
                break;
            case NO:
                break;
            case SOME:
                res = "some";
                break;
            case LONE:
                res = "lone";
                break;
            case ONE:
                res = "one";
                break;
            case TRANSPOSE:
                break;
            case RCLOSURE:
                break;
            case CLOSURE:
                break;
            case CARDINALITY:
                break;
            case CAST2INT:
                break;
            case CAST2SIGINT:
                break;
            case NOOP:
                res = x.sub.accept(this);
                break;
        }

        decDepth();
        return res;
    }

    @Override public String visit(Sig sig) throws Err {
        incDepth();
        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ sig.toString() + " Sig visited with depth " + getDepth());
        String res = null;
        if (sig.isSome != null) {
            res = "some";
        } else if (sig.isOne != null) {
            res = "one";
        } else if (sig.isLone != null) {
            res = "lone";
        }
        decDepth();
        return res;
    }

//    /** Visits an ExprBinary node (A OP B) by calling accept() on A then B. */
//    @Override public String visit(ExprBinary x) throws Err {
//        incDepth();
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprBinary visited with depth " + getDepth());
//        String res = null;
//        x.left.accept(this);
//        x.right.accept(this);
//        decDepth();
//        return res;
//    }

//    /** Visits an ExprList node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
//    @Override public String visit(ExprList x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprList visited with depth " + getDepth());
//        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
//        return null;
//    }
//
//    /** Visits an ExprCall node F[X1,X2,X3..] by calling accept() on X1, X2, X3... */
//    @Override public String visit(ExprCall x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprCall visited with depth " + getDepth());
//        for(Expr y:x.args) { String ans=y.accept(this); if (ans!=null) return ans; }
//        return null;
//    }
//
//    /** Visits an ExprConstant node (this default implementation simply returns null) */
//    @Override public String visit(ExprConstant x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprConstant visited with depth " + getDepth());
//        return null;
//    }
//
//    /** Visits an ExprITE node (C => X else Y) by calling accept() on C, X, then Y. */
//    @Override public String visit(ExprITE x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprITE visited with depth " + getDepth());
//        String ans = x.cond.accept(this);
//        if (ans==null) ans = x.left.accept(this);
//        if (ans==null) ans = x.right.accept(this);
//        return ans;
//    }
//
//    /** Visits an ExprLet node (let a=x | y) by calling accept() on "a", "x", then "y". */
//    @Override public String visit(ExprLet x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprLet visited with depth " + getDepth());
//        String ans = x.var.accept(this);
//        if (ans==null) ans = x.expr.accept(this);
//        if (ans==null) ans = x.sub.accept(this);
//        return ans;
//    }
//
//    /** Visits an ExprQt node (all a,b,c:X1, d,e,f:X2... | F) by calling accept() on a,b,c,X1,d,e,f,X2... then on F. */
//    @Override public String visit(ExprQt x) throws Err {
//        if(DEBUG) System.out.println("DEBUG(Multiplicity): "+ x.toString() + " ExprQt visited with depth " + getDepth());
//        for(Decl d: x.decls) {
//            for(ExprHasName v: d.names) { String ans = v.accept(this); if (ans!=null) return ans; }
//            String ans = d.expr.accept(this); if (ans!=null) return ans;
//        }
//        return x.sub.accept(this);
//    }
}
